import { filterApiClient } from "@lib/axios-client";
import { AxiosPromise } from "axios";
import { Service } from "../types/app";

export const getAllServicesFromFilterApi = (): AxiosPromise<Service[]> =>
  filterApiClient.request<Service[]>({
    method: "get",
    url: "/all-filterable-services",
  });
