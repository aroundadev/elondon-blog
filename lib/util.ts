import CategoryModel, { Category } from "@models/Category";
import { Post, PostModel } from "@models/Post";
import SettingsModel from "@models/Settings";
import {
  getAllCategories,
  getPosts,
  getPostsByCategory,
  getSettings,
} from "./api";
import { filterApiClient } from "@lib/axios-client";
import { AxiosPromise } from "axios";
import { Service } from "../types/app";

export const getPageNumber = (
  pageNumber: string | string[] | undefined
): number => {
  if (Array.isArray(pageNumber)) return 1;
  if (!parseInt(pageNumber)) return 1;
  return parseInt(pageNumber);
};

export const getCategoryData = async (): Promise<Category[]> => {
  const categoryData = await getAllCategories();
  const categories = categoryData.items.map((item) =>
    new CategoryModel(item).getSerializable()
  );
  return categories;
};

export const getPostsData = async (
  skip: number = 0
): Promise<[Post[], number]> => {
  let postData = await getPosts(skip);
  const posts = postData.items.map((item) =>
    new PostModel(item, postData.includes).getSerializable()
  );
  return [posts, postData.total];
};

export const getSettingsData = async (): Promise<SettingsModel> => {
  const settingsData = await getSettings();
  const settings = settingsData.items.map(
    (item) => new SettingsModel(item, settingsData.includes)
  )[0];
  return settings;
};

export const getPostsDataByCategory = async (
  slug: string,
  skip: number
): Promise<[Post[], number]> => {
  const data = await getPostsByCategory(slug, skip);
  const posts = data.items.map((item) =>
    new PostModel(item, data.includes).getSerializable()
  );
  return [posts, data.total];
};

export const getAllServices = async (): Promise<Service[]> => {
  const response = await filterApiClient.request<Service[]>({
    method: "get",
    url: "/all-filterable-services",
  });
  return response.data;
};
