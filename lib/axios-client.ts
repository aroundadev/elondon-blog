import axios from "axios";

const filterApiClient = axios.create({
  baseURL: process.env.NEXT_PUBLIC_FILTER_API_BASE_URL,
});

filterApiClient.interceptors.request.use(
  (config) => {
    console.info("Request:");
    console.info(config);
    return config;
  },
  (error) => {
    console.error(error);
    console.error("Response error:");
    return Promise.reject(error);
  }
);

filterApiClient.interceptors.response.use(
  (response) => {
    console.info("Response:");
    console.info(response);
    return response;
  },
  (error) => {
    console.error("Response error:");
    console.error(error);
    return Promise.reject(error);
  }
);

export { filterApiClient };
