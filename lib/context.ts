import React from "react";

const initalContext = {
  postsPerPage: 1,
  totalPosts: 1,
  currentPage: 1,
  currentCategory: "",
  setTotalPosts: (posts: number): void => {},
  setCurrentPage: (page: number): void => {},
  setCurrentCategory: (category: string): void => {},
};

export default React.createContext(initalContext);
