import {
  CategoryContentTypeData,
  EscortsData,
  PostsData,
} from "@types/app.types";
import { createClient } from "contentful";
import {
  CategoryResponse,
  ContentfulApiData,
  MainPageSettingsResponse,
  PageSettingsResponse,
  PostData,
  SettingsData,
} from "../types/app";

const config = {
  space: process.env.NEXT_PUBLIC_CONTENTFUL_SPACE_ID,
  accessToken:
    process.env.NEXT_PUBLIC_CONTENTFUL_ENVIRONMENT_ID === "test"
      ? process.env.NEXT_PUBLIC_CONTENTFUL_ACCESS_TOKEN_TEST
      : process.env.NEXT_PUBLIC_CONTENTFUL_ACCESS_TOKEN,
  environment: process.env.NEXT_PUBLIC_CONTENTFUL_ENVIRONMENT_ID,
};

const client = createClient(config);

export const getPosts = async (
  skip: number = 0,
  limit: number = parseInt(process.env.NEXT_PUBLIC_POSTS_PER_PAGE)
) => {
  console.log(config);
  const data = ((await client.getEntries({
    limit,
    skip,
    content_type: "blog",
    order: "-sys.createdAt",
  })) as unknown) as Promise<ContentfulApiData<PostData>>;
  return data;
};

export const getPost = async (slug: string) => {
  const data = ((await client.getEntries({
    content_type: "blog",
    "fields.postSlug": slug,
  })) as unknown) as Promise<ContentfulApiData<PostData>>;
  return data;
};

export const getPostsByCategory = async (
  slug: string,
  skip: number = 0,
  limit: number = parseInt(process.env.NEXT_PUBLIC_POSTS_PER_PAGE)
) => {
  const data = ((await client.getEntries({
    content_type: "blog",
    "fields.category.sys.contentType.sys.id": "blogCategory",
    "fields.category.fields.slug[match]": slug,
    order: "-sys.createdAt",
    skip,
    limit,
  })) as unknown) as Promise<ContentfulApiData<PostData>>;
  return data;
};

export const getAllCategories = async () => {
  const data = ((await client.getEntries({
    content_type: "blogCategory",
  })) as unknown) as Promise<CategoryContentTypeData>;
  return data;
};

export const getCategory = async (slug: string): Promise<PostsData> => {
  const data = ((await client.getEntries({
    content_type: "blogCategory",
    "fields.slug": slug,
  })) as unknown) as Promise<PostsData>;
  return data;
};

export const getRecommended = async () => {
  const data = ((await client.getEntries({
    content_type: "escorts",
    "fields.tags[in]": "Recommended",
    include: 2,
  })) as unknown) as Promise<EscortsData>;
  return data;
};

export const getPostsCreatedEarlier = async (date: string) => {
  const data = ((await client.getEntries({
    content_type: "blog",
    "sys.createdAt[lt]": date,
    order: "-sys.createdAt",
  })) as unknown) as Promise<ContentfulApiData<PostData>>;
  return data;
};

export const getPostsCreatedLater = async (date: string) => {
  const data = ((await client.getEntries({
    content_type: "blog",
    "sys.createdAt[gt]": date,
    order: "-sys.createdAt",
  })) as unknown) as Promise<ContentfulApiData<PostData>>;
  return data;
};

export const getSettings = async () => {
  const data = ((await client.getEntries({
    content_type: "blogSettings",
    include: 2,
  })) as unknown) as Promise<ContentfulApiData<SettingsData>>;
  return data;
};

export const getMainPageSettings = async () => {
  const data = ((await client.getEntries({
    content_type: "blogMainPageSettings",
  })) as unknown) as Promise<PageSettingsResponse>;
  return data;
};

export const getPostPageSettings = async () => {
  const data = ((await client.getEntries({
    content_type: "blogPostPageSettings",
  })) as unknown) as Promise<PageSettingsResponse>;
  return data;
};

export const getCategoryPageSettings = async (slug: string) => {
  const data = ((await client.getEntries({
    content_type: "blogCategory",
    "fields.slug": slug,
  })) as unknown) as Promise<CategoryResponse>;
  return data;
};
