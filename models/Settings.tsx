import {
  EntryData,
  SettingsData,
  LinkedEntry,
  Includes,
  PostData,
} from "../types/app";
import { Post, PostModel } from "./Post";

export interface Settings {
  slideshow: Post[];
  mainPageTitle: string;
  mainPageDescription: string;
  categoryPageDescription: string;
  categoryPageTitle: string;
}

export default class SettingsModel {
  private baseUrl: string;
  private slideshow: LinkedEntry[];
  private includes: Includes;
  private mainPageTitle: string;
  private mainPageDescription: string;
  private categoryPageTitle: string;
  private categoryPageDescription: string;

  constructor(item: EntryData<SettingsData>, includes: Includes) {
    this.slideshow = item.fields.slideshowItems;
    this.baseUrl = item.fields.baseUrl;
    this.includes = includes;
    this.mainPageTitle = item.fields.mainPageTitle;
    this.mainPageDescription = item.fields.mainPageDescription;
    this.categoryPageTitle = item.fields.categoryPageTitle;
    this.categoryPageDescription = item.fields.categoryPageDescription;
  }

  getSerializable(): Settings {
    return {
      slideshow: this.getSlideshow(),
      mainPageTitle: this.getMainPageTitle(),
      mainPageDescription: this.getMainPageDescription(),
      categoryPageTitle: this.getCategoryPageTitle(),
      categoryPageDescription: this.getCategoryPageDescription(),
    };
  }

  private getSlideshowIncludes(): EntryData<PostData>[] {
    if (!this.slideshow || !this.includes || !this.includes.Entry) return null;
    const postData = this.slideshow.map((item) => {
      const include = this.includes.Entry.find((entry) => {
        return entry.sys.id === item.sys.id;
      }) as EntryData<PostData>;
      return include;
    });
    //filter undefined values and return
    return postData.filter((p) => !!p);
  }

  getSlideshow(): Post[] {
    const postData = this.getSlideshowIncludes();
    if (!postData) return [];
    const posts = postData.map((item) => {
      const post = new PostModel(item, this.includes).getSerializable();
      return post;
    });
    return posts;
  }

  getMainPageTitle() {
    return this.mainPageTitle || "";
  }

  getMainPageDescription() {
    return this.mainPageDescription || "";
  }

  getCategoryPageTitle() {
    return this.categoryPageTitle || "";
  }

  getCategoryPageDescription() {
    return this.categoryPageDescription || "";
  }
}
