import {
  PageSettingsResponse,
  PageSettingsFields,
  PageSettings,
} from "@types/app";

export default class PageSettingsModel {
  data: PageSettingsFields;

  constructor(data: PageSettingsResponse) {
    this.data = data?.items[0]?.fields;
  }

  getSerializable(): PageSettings {
    return {
      titleMetaTag: this.titleMetaTag,
      metaDescriptionTag: this.metaDescriptionTag,
      latestPostsTitle: this.latestPostsTitle,
      subscribeTitle: this.subscribeTitle,
      subscribeText: this.subscribeText,
      subscribeButton: this.subscribeButton,
      callToActionTitle: this.callToActionTitle,
      callToActionText: this.callToActionText,
      callToActionButtonWrapperTitle: this.callToActionButtonWrapperTitle,
      callToActionButtonWrapperText: this.callToActionButtonWrapperText,
      phone: this.phone,
      phoneUrl: this.phoneUrl,
      whatsApp: this.whatsApp,
      whatsAppUrl: this.whatsAppUrl,
      callToActionFooterSiteDescription: this.callToActionFooterSiteDescription,
      callToActionFooterTitle: this.callToActionFooterTitle,
      callToActionFooterText: this.callToActionFooterText,
      termsFooter: this.termsFooter,
      featuredPostsTitle: this.featuredPostsTitle,
      recommendedLine1: this.recommendedLine1,
      recommendedLine2: this.recommendedLine2,
    };
  }
  get titleMetaTag(): string {
    return this.data.titleMetaTag || "";
  }
  get metaDescriptionTag(): string {
    return this.data.metaDescriptionTag || "";
  }
  get latestPostsTitle(): string {
    return this.data.latestPostsTitle || "";
  }
  get subscribeTitle(): string {
    return this.data.subscribeTitle || "";
  }
  get subscribeText(): string {
    return this.data.subscribeText || "";
  }
  get subscribeButton(): string {
    return this.data.subscribeButton || "";
  }
  get callToActionTitle(): string {
    return this.data.callToActionTitle || "";
  }
  get callToActionText(): string {
    return this.data.callToActionText || "";
  }
  get callToActionButtonWrapperTitle(): string {
    return this.data.callToActionButtonWrapperTitle || "";
  }
  get callToActionButtonWrapperText(): string {
    return this.data.callToActionButtonWrapperText || "";
  }
  get phone(): string {
    return this.data.phone || "";
  }
  get whatsApp(): string {
    return this.data.whatsApp || "";
  }

  get phoneUrl(): string {
    return `tel:+${this.phone.replace(/ /g, "")}`;
  }

  get whatsAppUrl(): string {
    return `https://wa.me/${this.whatsApp
      .replace("(", "")
      .replace(")", "")
      .replace("+", "")
      .replace(/ /g, "")}`;
  }

  get callToActionFooterSiteDescription(): string {
    return this.data.callToActionFooterSiteDescription || "";
  }
  get callToActionFooterTitle(): string {
    return this.data.callToActionFooterTitle || "";
  }
  get callToActionFooterText(): string {
    return this.data.callToActionFooterText || "";
  }
  get termsFooter(): string {
    return this.data.termsFooter || "";
  }

  get featuredPostsTitle(): string {
    return this.data.featuredPostsTitle || "";
  }
  get recommendedLine1(): string {
    return this.data.recommendedLine1 || "";
  }
  get recommendedLine2(): string {
    return this.data.recommendedLine2 || "";
  }
}
