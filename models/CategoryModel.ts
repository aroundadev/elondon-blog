import { Category, CategoryData, CategoryResponse } from "@types/app";

export default class CategoryModel {
  private data: CategoryData;

  constructor(data: CategoryResponse) {
    this.data = data.items[0].fields;
  }

  getSerializable(): Category {
    return {
      title: this.title,
      slug: this.slug,
      titleMetaTag: this.titleMetaTag,
      metaDescriptionTag: this.metaDescriptionTag,
    };
  }

  get title(): string {
    return this.data.title || "";
  }

  get slug(): string {
    return this.data.slug || "";
  }

  get titleMetaTag(): string {
    return this.data.titleMetaTag || "";
  }

  get metaDescriptionTag(): string {
    return this.data.metaDescriptionTag || "";
  }
}
