import { CategoryContentTypeEntryData } from "types/app.types";

export interface Category {
  name: string;
  slug: string;
}

export default class CategoryModel {
  private name: string;
  private slug: string;

  constructor(entry: CategoryContentTypeEntryData) {
    this.name = entry.fields.title;
    this.slug = entry.fields.slug;
  }

  getSerializable(): Category {
    return {
      name: this.getName(),
      slug: this.getSlug(),
    };
  }

  getName() {
    return this.name;
  }

  getSlug() {
    return this.slug;
  }
}
