import { uuid } from "uuidv4";
import {
  EscortsData,
  EscortData,
  ImageData,
  RatesData,
  LocationData,
} from "../types/app.types";

export interface Escort {
  id: string;
  title?: string;
  imageUrl?: string;
  incallPrice?: string;
  outcallPrice?: string;
  location?: string;
  newTag?: boolean;
  recommendedTag?: boolean;
  slug?: string;
}

export default class EscortModel {
  private id: string;
  private title?: string;
  private photos?: ImageData[];
  private rates?: RatesData;
  private location?: LocationData;
  private tags?: string[];
  private escortsData?: EscortsData;

  constructor(escortData: EscortData, escortsData: EscortsData) {
    this.id = uuid();
    this.title = escortData.fields.title;
    this.photos = escortData.fields.photos;
    this.rates = escortData.fields.rates;
    this.location = escortData.fields.location;
    this.tags = escortData.fields.tags;
    this.escortsData = escortsData;
  }

  getSerializable(): Escort {
    return {
      id: this.getId(),
      title: this.getTitle(),
      imageUrl: this.getThumbnail(),
      incallPrice: this.getIncallPrice(),
      outcallPrice: this.getOutcallPrice(),
      location: this.getLocation(),
      newTag: this.getTag("new") ? true : false,
      recommendedTag: this.getTag("recommended") ? true : false,
      slug: this.getSlug(),
    };
  }

  getId() {
    return this.id;
  }

  getTitle() {
    return this.title;
  }

  getSlug() {
    return this.title.toLowerCase();
  }

  getThumbnail(): string {
    const thumbId = this.photos[0].sys.id;
    const thumbUrl = this.escortsData.includes.Asset?.find(
      (item) => item.sys.id === thumbId
    );
    return `https:${thumbUrl?.fields.file.url}`;
  }

  getIncallPrice() {
    return this.rates.gbp["1_One hour"].incall;
  }

  getOutcallPrice() {
    return this.rates.gbp["1_One hour"].outcall;
  }

  getLocation() {
    return this.location.name;
  }

  getTag(name: string) {
    return this.tags.find((tag) => tag.toLowerCase() === name.toLowerCase());
  }
}
