import moment from "moment";
import { EntryFields } from "contentful";
import {
  PostData,
  EntryData,
  Includes,
  LinkedEntry,
  Content,
  CategoryData,
  AuthorData,
  AssetField,
} from "../types/app";

export interface Post {
  title: string;
  author: string;
  excerpt: string;
  date: string;
  category: string;
  thumbnail: string;
  headerImage: string;
  content?: string;
  slug: string;
  titleMetaTag: string;
  metaDescriptionTag: string;
}

export class PostModel {
  private title?: string;
  private author?: LinkedEntry;
  private category?: LinkedEntry;
  private createdAt?: string;
  private updatedAt?: string;
  private excerpt?: string;
  private content?: Content;
  private includes?: Includes;
  private thumbnail?: LinkedEntry;
  private headerImage?: LinkedEntry;
  private slug?: string;
  private POST_DATE_FORMAT = "MMM D, YYYY";
  private DEFAULT_IMAGE =
    "https://via.placeholder.com/600x400.png?text=No%20image";
  private titleMetaTag?: string;
  private metaDescriptionTag?: string;

  constructor(data: EntryData<PostData>, includes: Includes) {
    this.title = data.fields.title;
    this.author = data.fields.author;
    this.category = data.fields.category;
    this.createdAt = data.sys.createdAt;
    this.updatedAt = data.sys.updatedAt;
    this.excerpt = data.fields.excerpt;
    this.includes = includes;
    this.thumbnail = data.fields.thumbnail;
    this.headerImage = data.fields.headerImage;
    this.slug = data.fields.postSlug;
    this.content = data.fields.content;
    this.titleMetaTag = data.fields.titleMetaTag;
    this.metaDescriptionTag = data.fields.metaDescriptionTag;
  }

  getSerializable(): Post {
    return {
      title: this.getTitle(),
      author: this.getAuthor(),
      excerpt: this.getEscerpt(),
      date: this.getDate(),
      category: this.getCategory(),
      thumbnail: this.getThumbnail(),
      headerImage: this.getHeaderImage(),
      slug: this.getSlug(),
      content: this.getContent(),
      titleMetaTag: this.getTitleMetaTag(),
      metaDescriptionTag: this.getMetaDescriptionTag(),
    };
  }

  getContent() {
    const dummy = JSON.stringify({
      data: {},
      marks: [],
      value: "",
      nodeType: "text",
    });
    return this.content ? JSON.stringify(this.content) : dummy;
  }

  getTitle() {
    return this.title || "";
  }

  getTitleMetaTag(): string {
    return this.titleMetaTag || "";
  }

  getMetaDescriptionTag(): string {
    return this.metaDescriptionTag || "";
  }

  getSlug() {
    return this.slug || "";
  }

  getEscerpt() {
    return this.excerpt || "";
  }

  getDate(dateFormat = this.POST_DATE_FORMAT) {
    return moment(this.createdAt).format(dateFormat);
  }

  getAuthor(): string {
    const authorData = this.getLinkedEntry("author") as AuthorData;
    return (authorData && authorData.name) || "";
  }

  getCategory(): string {
    const categoryData = this.getLinkedEntry("category") as CategoryData;
    return (categoryData && categoryData.title) || "";
  }

  getThumbnail(): string {
    if (!this.thumbnail || !this.includes || !this.includes.Asset) {
      return this.DEFAULT_IMAGE;
    }
    return this.getAssetByField("thumbnail");
  }

  getHeaderImage(): string {
    if (!this.headerImage || !this.includes || !this.includes.Asset) {
      return this.DEFAULT_IMAGE;
    }
    return this.getAssetByField("headerImage");
  }

  private getLinkedEntry(name: string) {
    if (!this[name]) {
      return "";
    }
    if (!(this.includes && this.includes.Entry)) return "";

    const entryId = this[name]?.sys?.id;
    const found = this.includes.Entry.find((entry) => {
      return entry.sys.id === entryId;
    });
    return found?.fields;
  }

  private getAssetByField(name: AssetField): string {
    const id = this[name].sys.id;
    const found = this.includes.Asset.find((asset) => asset.sys.id === id)
      ?.fields.file.url;
    return `https:${found}`;
  }
}
