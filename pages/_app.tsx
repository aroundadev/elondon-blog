import "../styles/globals.css";
import AppContext from "../lib/context";
import { useState } from "react";

function MyApp({ Component, pageProps }) {
  const [totalPosts, setTotalPosts] = useState(0);
  const [pageNumber, setPageNumber] = useState(1);
  const [currentCategory, setCurrentCategory] = useState("");
  return (
    <AppContext.Provider
      value={{
        totalPosts,
        currentPage: pageNumber,
        currentCategory,
        postsPerPage: parseInt(process.env.NEXT_PUBLIC_POSTS_PER_PAGE),
        setTotalPosts: (posts: number) => setTotalPosts(posts),
        setCurrentPage: (pageNumber: number) => setPageNumber(pageNumber),
        setCurrentCategory: (category: string) => setCurrentCategory(category),
      }}
    >
      <Component {...pageProps} />;
    </AppContext.Provider>
  );
}

export default MyApp;
