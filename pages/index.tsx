import Layout from "@components/sections/Layout";
import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import BlogMain from "@pages/Blog/BlogMain";
import AppContext from "@lib/context";
import React, { useContext, useEffect, useState } from "react";
import Head from "next/head";
import {
  getCategoryData,
  getPageNumber,
  getPostsData,
  getSettingsData,
} from "@lib/util";
import BlogPagination from "@components/simple/BlogPagination";
import BlogPostsHeader from "@components/simple/BlogPostsHeader";
import { getMainPageSettings } from "@lib/api";
import PageSettingsModel from "@models/PageSettingsModel";
import { PageSettings } from "@types/app";
import { Post } from "@models/Post";
import BlogSlider from "@components/pages/Blog/BlogSlider";
import BlogPosts from "@components/pages/Blog/BlogPosts";
import BlogMore from "@components/pages/Blog/BlogMore";
import GirlsWaiting from "@components/sections/GirlsWaiting";

export const getServerSideProps: GetServerSideProps<{
  posts: Post[];
  slideshow: Post[];
  totalPosts: number;
  pageNumber: number;
  pageSettings: PageSettings;
}> = async ({ query }) => {
  const pageNumber = getPageNumber(query.p);
  const postsPerPage = parseInt(process.env.NEXT_PUBLIC_POSTS_PER_PAGE);
  const skip = (pageNumber - 1) * postsPerPage;
  try {
    const [posts, totalPosts] = await getPostsData(skip);
    const settings = await getSettingsData();
    const { slideshow } = settings.getSerializable();

    const mainPageSettingsData = await getMainPageSettings();
    const pageSettings = new PageSettingsModel(
      mainPageSettingsData
    ).getSerializable();

    return {
      props: {
        posts,
        slideshow,
        pageNumber,
        totalPosts,
        pageSettings,
      },
    };
  } catch (e) {
    console.error(e);
    return {
      props: {
        posts: [],
        slideshow: [],
        pageNumber: 1,
        totalPosts: 0,
        pageSettings: {},
      },
    };
  }
};

export default function Home({
  posts,
  slideshow,
  totalPosts,
  pageNumber,
  pageSettings,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const { setTotalPosts, setCurrentPage } = useContext(AppContext);
  const [categories, setCategories] = useState([]);
  useEffect(() => {
    setTotalPosts(totalPosts);
    getCategoryData().then((data) => {
      setCategories(data);
    });
  }, []);

  useEffect(() => {
    setCurrentPage(pageNumber);
  }, [pageNumber]);

  const { titleMetaTag, metaDescriptionTag, latestPostsTitle } = pageSettings;

  return (
    <Layout pageSettings={pageSettings}>
      <Head>
        <title>{titleMetaTag}</title>
        <meta name="description" content={metaDescriptionTag} />
      </Head>

      {slideshow && slideshow.length > 0 && <BlogSlider posts={slideshow} />}

      <BlogPosts
        posts={posts}
        categories={categories}
        header={
          <BlogPostsHeader
            title={latestPostsTitle || "Latest posts"}
            categories={categories}
          />
        }
        pagination={<BlogPagination pageType="main" />}
      />
      <BlogMore pageSettings={pageSettings} />
      <GirlsWaiting pageSettings={pageSettings} />
      {/*} 
      <BlogMain
        posts={posts}
        slideshow={slideshow}
        categories={categories}
        header={
          <BlogPostsHeader
            title={latestPostsTitle || "Latest posts"}
            categories={categories}
          />
        }
        pagination={<BlogPagination pageType="main" />}
      />
    {*/}
    </Layout>
  );
}
