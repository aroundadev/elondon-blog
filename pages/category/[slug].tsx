import Layout from "@components/sections/Layout";
import { GetServerSideProps } from "next";
import { Post } from "models/Post";
import BlogMain from "@pages/Blog/BlogMain";
import AppContext from "@lib/context";
import React, { useContext, useEffect, useState } from "react";
import { ParsedUrlQuery } from "querystring";
import {
  getCategoryData,
  getPageNumber,
  getPostsDataByCategory,
  getSettingsData,
} from "@lib/util";
import BlogPagination from "@components/simple/BlogPagination";
import BlogPostsHeader from "components/simple/BlogPostsHeader";
import Head from "next/head";
import { getCategoryPageSettings, getMainPageSettings } from "@lib/api";
import PageSettingsModel from "@models/PageSettingsModel";
import { Category, PageSettings } from "@types/app";
import BlogSlider from "@components/pages/Blog/BlogSlider";
import BlogPosts from "@components/pages/Blog/BlogPosts";
import BlogMore from "@components/pages/Blog/BlogMore";
import GirlsWaiting from "@components/sections/GirlsWaiting";
import CategoryModel from "@models/CategoryModel";

interface Props {
  posts: Post[];
  slideshow: Post[];
  category: string;
  totalPosts: number;
  slug: string;
  pageNumber: number;
  mainPageSettings: PageSettings;
  categorySettings: Category;
}

export const getServerSideProps: GetServerSideProps<Props> = async ({
  query,
  params,
}) => {
  const pageNumber = getPageNumber(query.p);
  const postsPerPage = parseInt(process.env.NEXT_PUBLIC_POSTS_PER_PAGE);
  const skip = (pageNumber - 1) * postsPerPage;
  const { slug } = params;

  try {
    const [posts, totalPosts] = await getPostsDataByCategory(slug, skip);
    const slideshow = (await getSettingsData()).getSlideshow();
    const category = posts[0].category;

    const mainPageSettingsData = await getMainPageSettings();

    const mainPageSettings = new PageSettingsModel(
      mainPageSettingsData
    ).getSerializable();

    const categorySettingsData = await getCategoryPageSettings(slug as string);
    const categorySettings = new CategoryModel(
      categorySettingsData
    ).getSerializable();

    return {
      props: {
        posts,
        slideshow,
        category,
        totalPosts,
        pageNumber,
        slug,
        mainPageSettings,
        categorySettings,
      },
    };
  } catch {
    return {
      props: {
        posts: [],
        slideshow: [],
        category: "",
        totalPosts: 0,
        pageNumber: 1,
        slug: "",
        mainPageSettings: {},
        categorySettings: {},
      },
    };
  }
};
export default function Home({
  posts,
  slideshow,
  category,
  totalPosts,
  slug,
  pageNumber,
  mainPageSettings,
  categorySettings,
}: Props) {
  const { setTotalPosts, setCurrentPage, setCurrentCategory } = useContext(
    AppContext
  );
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    setTotalPosts(totalPosts);
    getCategoryData().then((data) => setCategories(data));
  }, []);

  useEffect(() => {
    setCurrentPage(pageNumber);
  }, [pageNumber]);

  useEffect(() => {
    setCurrentCategory(category);
  }, [category]);

  return (
    <Layout pageSettings={mainPageSettings}>
      <Head>
        <title>{categorySettings.titleMetaTag}</title>
        <meta
          name="description"
          content={categorySettings.metaDescriptionTag}
        />
      </Head>

      {slideshow && slideshow.length > 0 && <BlogSlider posts={slideshow} />}

      <BlogPosts
        posts={posts}
        categories={categories}
        header={
          <BlogPostsHeader
            currentCategory={category}
            categories={categories}
            title={`${category}`}
          />
        }
        pagination={<BlogPagination pageType="category" slug={slug} />}
      />
      <BlogMore pageSettings={mainPageSettings} />
      <GirlsWaiting pageSettings={mainPageSettings} />

      {/*}
      <BlogMain
        posts={posts}
        slideshow={slideshow}
        categories={categories}
        header={
          <BlogPostsHeader
            currentCategory={category}
            categories={categories}
            title={`${category}`}
          />
        }
        pagination={}
      />
      {*/}
    </Layout>
  );
}

/*
export const getStaticPaths: GetStaticPaths = async () => {
  const data = await getAllCategories();
  const paths: PathsItem[] = data.items.map((item) => ({
    params: { slug: item.fields.slug },
  }));
  return {
    paths,
    fallback: false,
  };
};

export async function getStaticProps() {
  const data = await getPosts();
  const posts = data.items.map((item) =>
    new PostModel(item, data).getSerializable()
  );
  return {
    props: {
      posts,
      totalPosts: data.total,
    },
    revalidate: 1,
  };
}
 */
