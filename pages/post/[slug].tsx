import Layout from "@components/sections/Layout";
import BlogMore from "@pages/Blog/BlogMore";
import GirlsWaiting from "@components/sections/GirlsWaiting";
import Recommended from "@components/pages/Main/Recommended";
import {
  getMainPageSettings,
  getPost,
  getPostPageSettings,
  getPosts,
  getPostsCreatedEarlier,
  getPostsCreatedLater,
  getRecommended,
} from "@lib/api";
import Head from "next/head";
import { useRouter } from "next/router";
import { PostModel, Post } from "@models/Post";
import { useEffect, useState } from "react";
import EscortModel, { Escort } from "@models/Escort";
import PostContent from "@components/pages/Blog/BlogPost/PostContent";
import PostSlider from "@components/pages/Blog/BlogPost/PostSlider";
import Link from "next/link";
import ProfileNav from "@components/simple/ProfileNav";
import s from "./style.module.css";
import ArrowLeft from "@svg/arrow-left.svg";
import { GetStaticProps } from "next";
import PageSettingsModel from "@models/PageSettingsModel";
import { PageSettings } from "@types/app";

interface Props {
  data: Post;
  prevPostSlug: string;
  nextPostSlug: string;
  pageSettings: PageSettings;
}

export async function getStaticPaths() {
  const data = await getPosts(0, 1000);
  const paths = data.items.map((item) => ({
    params: {
      slug: item.fields.postSlug,
    },
  }));
  return {
    paths,
    fallback: true,
  };
}

export const getStaticProps: GetStaticProps<Props> = async ({
  params: { slug },
}) => {
  const postPageSettingsData = await getPostPageSettings();
  const pageSettings = new PageSettingsModel(
    postPageSettingsData
  ).getSerializable();

  const mainPageSettingsData = await getMainPageSettings();
  const mainPageSettings = new PageSettingsModel(
    mainPageSettingsData
  ).getSerializable();

  pageSettings.phone = mainPageSettings.phone;
  pageSettings.whatsApp = mainPageSettings.whatsApp;

  const data = await getPost(slug as string);
  const post = new PostModel(data.items[0], data.includes).getSerializable();

  const dateCreated = data.items[0].sys.createdAt;
  const prevPostSlug = await getPrevPostSlug(dateCreated);
  const nextPostSlug = await getNextPostSlug(dateCreated);

  return {
    props: { data: post, prevPostSlug, nextPostSlug, pageSettings },
    revalidate: 1,
  };
};

export default function PostPage({
  data,
  prevPostSlug,
  nextPostSlug,
  pageSettings,
}: Props) {
  const [recommended, setRecommended] = useState<Escort[]>([]);
  const [featured, setFeatured] = useState<Post[]>([]);
  const router = useRouter();

  useEffect(() => {
    (async () => {
      const recommendedData = await getRecommended();
      const escorts = recommendedData.items.map((item) =>
        new EscortModel(item, recommendedData).getSerializable()
      );
      setRecommended(escorts);

      const postsData = await getPosts();
      const featuredPosts = postsData.items.map((item) =>
        new PostModel(item, postsData.includes).getSerializable()
      );
      setFeatured(featuredPosts);
    })();
  }, []);

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <Layout pageSettings={pageSettings}>
      <Head>
        <title>{`${data.titleMetaTag} | Escortslondon blog`}</title>
        <meta name="description" content={data.metaDescriptionTag} />
      </Head>
      <section className={s.blogPost}>
        <section className={s.nav}>
          <div className={s.container}>
            <div className={s.topNav}>
              <div className={s.topNavSide}>
                <Link href="/">
                  <a className={`${s.navLink} ${s.left}`}>
                    <ArrowLeft />
                    Back to Blog
                  </a>
                </Link>
              </div>
              <div className={`${s.topNavSide} ${s.alignRight}`}>
                <ProfileNav
                  prev={nextPostSlug}
                  next={prevPostSlug}
                  cleanCurrentProfileData={() => {}}
                />
              </div>
            </div>
          </div>
        </section>
        <PostContent data={data} />
        <PostSlider title={pageSettings.featuredPostsTitle} posts={featured} />
        <GirlsWaiting pageSettings={pageSettings} />
        <Recommended pageSettings={pageSettings} data={recommended} />
        <BlogMore pageSettings={pageSettings} />
      </section>
    </Layout>
  );
}

async function getPrevPostSlug(date: string) {
  let prevPostSlug = "";
  const prevPosts = await getPostsCreatedEarlier(date);

  if (prevPosts.items.length !== 0) {
    prevPostSlug = prevPosts.items[0].fields.postSlug;
  }
  return prevPostSlug;
}

async function getNextPostSlug(date: string) {
  let nextPostSlug = "";
  const nextPosts = await getPostsCreatedLater(date);

  if (nextPosts.items.length !== 0) {
    const lastElemIndex = nextPosts.items.length - 1;
    nextPostSlug = nextPosts.items[lastElemIndex].fields.postSlug;
  }

  return nextPostSlug;
}
