import React, { useEffect, useMemo, useState, FC } from "react";
import noScroll from "no-scroll";
import { useMediaQuery } from "react-responsive";
import Header from "components/sections/Header";
import Aside from "components/sections/Aside";
import Footer from "components/sections/Footer";
import { getAllServices } from "@lib/util";
import s from "./style.module.css";
import { Menu } from "@lib/Menu";
import PageSettings from "@models/PageSettings";

interface Props {
  pageSettings: PageSettings;
}

const Layout: FC<Props> = ({ children, pageSettings }) => {
  const [toggleAside, setToggleAside] = useState(false);
  const isTablet = useMediaQuery({ query: "(max-width: 1240px)" });

  const [servicesSubMenu, setServicesSubMenu] = useState([]);

  useEffect(() => {
    (async () => {
      const services = await getAllServices();
      setServicesSubMenu(services);
    })();
  }, []);

  const menu = useMemo(() => {
    const menu = new Menu();
    menu.addSubMenu("services", servicesSubMenu);
    return menu.items;
  }, [servicesSubMenu]);
  const handleToggleAside = () => {
    setToggleAside(!toggleAside);
    noScroll.toggle();
  };
  return (
    <>
      <main className={s.layout}>
        <Header
          toggleAside={toggleAside}
          handleToggleAside={handleToggleAside}
          menu={menu}
          pageSettings={pageSettings}
        />
        {isTablet ? (
          <Aside
            toggleAside={toggleAside}
            handleToggleAside={handleToggleAside}
            menu={menu}
          />
        ) : null}
        {children}
        <Footer pageSettings={pageSettings} />
      </main>
    </>
  );
};
export default Layout;
