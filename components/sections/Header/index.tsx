import React, { useState } from "react";
import cx from "classnames";
import PropTypes from "prop-types";
import Button from "@components/simple/Button";
import Logo from "@svg/el-logo.svg";
import Link from "next/link";
import Arrow from "@svg/arrow-right.svg";
import Burger from "@svg/burger.svg";
import BurgerClose from "@svg/close-burger.svg";
import s from "./style.module.css";
import { MenuItem, PageSettings } from "../../../types/app";
import ArrowDown from "@svg/arrow-down.svg";

const baseUrl = process.env.NEXT_PUBLIC_BASE_URL;

interface Props {
  toggleAside: boolean;
  handleToggleAside(): void;
  menu: MenuItem[];
  pageSettings: PageSettings;
}

const Header = ({
  pageSettings,
  menu,
  toggleAside,
  handleToggleAside,
}: Props) => {
  const [submenuIsOpen, setSubmenuIsOpen] = useState(false);

  const handleSubmenuContainerClick = () => {
    setSubmenuIsOpen(!submenuIsOpen);
  };

  const { phone, phoneUrl } = pageSettings;

  return (
    <header className={`${s.header} ${toggleAside ? s.openMenu : ""}`}>
      <div className={s.container}>
        <div className={s.leftRightSideWrapper}>
          <div className={s.leftSide}>
            <Link href={`${baseUrl}`}>
              <a className={s.logo}>
                <Logo />
              </a>
            </Link>
            <nav className={s.navContainer}>
              <ul className={s.menu}>
                {menu.map(({ title, slug, subMenu }) =>
                  subMenu ? (
                    <li key={slug}>
                      <button
                        className={s.menuItemWithSubmenu}
                        onClick={handleSubmenuContainerClick}
                        type="button"
                      >
                        {title}
                        <ArrowDown className={s.submenuArrow} />
                      </button>
                      <ul className={s.servicesLinks}>
                        {subMenu.map(({ title, slug }) => (
                          <li key={slug}>
                            <a href={`${baseUrl}/services/${slug}`}>{title}</a>
                          </li>
                        ))}
                      </ul>
                    </li>
                  ) : (
                    <li key={slug}>
                      <a href={`${baseUrl}${slug}`}>{title}</a>
                    </li>
                  )
                )}
              </ul>
            </nav>
          </div>
          <div className={s.rightSide}>
            <div
              className={cx(s.contactWrapper, toggleAside && "asideMenuIsOpen")}
            >
              <img className={s.contactImage} src="/blog/lips.png" alt="lips" />
              <div className={s.contact}>
                <p className={s.contactLabel}>Calls and SMS</p>
                <a className={s.contactTel} href={phoneUrl}>
                  {phone}
                </a>
              </div>
            </div>
            <div className={s.dateWrapper}>
              <Button link={`${baseUrl}/make-a-date`} theme="outline">
                Make a date
                <Arrow />
              </Button>
            </div>
            <button
              type="button"
              className={s.burger}
              onClick={handleToggleAside}
            >
              {toggleAside ? <BurgerClose /> : <Burger />}
            </button>
          </div>
        </div>
        {/* <Subheader className={s.subheader} /> */}
      </div>
    </header>
  );
};

export default Header;
