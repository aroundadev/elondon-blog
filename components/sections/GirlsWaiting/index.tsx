import React, { FC } from "react";
import PropTypes from "prop-types";

import Title from "@simple/Title";
import Button from "@simple/Button";

import Phone from "@svg/phone.svg";
import Messenger from "@svg/messenger.svg";

import s from "./style.module.css";
import { PageSettings } from "@types/app";

const baseUrl = process.env.NEXT_PUBLIC_BASE_URL;

interface Props {
  pageSettings: PageSettings;
  bgColor?: string;
}

const GirlsWaiting: FC<Props> = ({ bgColor = "", pageSettings }) => {
  const handleButtonClick = () => {};
  const {
    callToActionTitle,
    callToActionText,
    callToActionButtonWrapperTitle,
    callToActionButtonWrapperText,
    phone,
    phoneUrl,
    whatsApp,
    whatsAppUrl,
  } = pageSettings;

  return (
    <section
      className={`
        ${s.girlsWaiting}
        ${bgColor === "black" ? s.girlsWaitingBlack : ""}
      `}
    >
      <div className={s.container}>
        <div className={s.content}>
          <div className={s.contentTitle}>
            <Title size="h4">
              {callToActionTitle || `Girls are waiting for you!`}
            </Title>
            <p>
              {callToActionText ||
                `Call Escorts London today to meet the hottest local babes`}
            </p>
          </div>
          <div className={s.contactWrapper}>
            <a className={s.contactPhone} href={phoneUrl}>
              <Phone />
              <span>{phone}</span>
            </a>
            <a className={s.contactPhone} href={whatsAppUrl}>
              <Messenger />
              <span>{whatsApp}</span>
            </a>
          </div>
        </div>
        <div className={s.makeDateWrapper}>
          <div className={s.makeDateTitle}>
            <Title size="h4">
              {callToActionButtonWrapperTitle || "Day & Night"}
            </Title>
            <p>
              {callToActionButtonWrapperText ||
                "We are open from 10am - 2am for incalls and outcalls"}
            </p>
          </div>
          <div className={s.btnWrapper}>
            <Button
              link={`${baseUrl}/make-a-date`}
              onClick={handleButtonClick}
              size="sm"
              theme="light"
            >
              Make a Date
            </Button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default GirlsWaiting;
