import { useState } from "react";
import Arrow from "@svg/arrow-right.svg";
import ArrowDown from "@svg/arrow-down.svg";
import Button from "@components/simple/Button";
import s from "./style.module.css";
import { MenuItem } from "../../../types/app";

const baseUrl = process.env.NEXT_PUBLIC_BASE_URL;

interface Props {
  menu: MenuItem[];
  handleToggleAside(): void;
  toggleAside: boolean;
}

const Aside = ({ menu, toggleAside, handleToggleAside }: Props) => {
  const [submenuIsOpen, setSubmenuIsOpen] = useState(false);

  const handleClick = () => {
    handleToggleAside();
  };
  const handleSubmenuContainerClick = () => {
    setSubmenuIsOpen(!submenuIsOpen);
  };

  return (
    <aside
      className={`${s.aside} ${toggleAside ? s.active : ""}`}
      onClick={handleClick}
    >
      <div className={s.menuWrapper} onClick={(e) => e.stopPropagation()}>
        <ul>
          {menu.map(({ title, slug, subMenu }) =>
            subMenu ? (
              <li
                onClick={handleSubmenuContainerClick}
                className={s.submenuContainer}
                key={slug}
              >
                <button type="button">
                  {title}&nbsp;{" "}
                  <ArrowDown className={submenuIsOpen ? s.rotate180 : false} />
                </button>
                <ul className={s.submenu}>
                  {submenuIsOpen &&
                    subMenu.map(({ title, slug }) => (
                      <li key={title}>
                        <a
                          onClick={handleClick}
                          href={`${baseUrl}/services/${slug}`}
                        >
                          {title}
                        </a>
                      </li>
                    ))}
                </ul>
              </li>
            ) : (
              <li key={slug}>
                {title === "Blog" ? (
                  <a onClick={handleToggleAside} href={`${baseUrl}${slug}`}>
                    {title}
                  </a>
                ) : (
                  <a onClick={handleClick} href={`${baseUrl}${slug}`}>
                    {title}
                  </a>
                )}
              </li>
            )
          )}
          <li className={s.buttonContainer}>
            <Button>
              <a onClick={handleClick} href={`${baseUrl}/make-a-date`}>
                Make a date
              </a>
              <Arrow />
            </Button>
          </li>
        </ul>
      </div>
    </aside>
  );
};

export default Aside;
