import React, { FC } from "react";

import Button from "@simple/Button";

import Link from "next/link";

import Phone from "@svg/phone.svg";
import Messenger from "@svg/messenger.svg";
import Logo from "@svg/logo-full.svg";

import s from "./style.module.css";
import PageSettings from "@models/PageSettings";

const baseUrl = process.env.NEXT_PUBLIC_BASE_URL;

interface Props {
  pageSettings: PageSettings;
}

const Footer: FC<Props> = ({ pageSettings }) => {
  const handleMakeDateClick = () => {};
  const {
    callToActionFooterSiteDescription,
    callToActionFooterTitle,
    callToActionFooterText,
    termsFooter,
    phoneUrl,
    phone,
    whatsAppUrl,
    whatsApp,
  } = pageSettings;
  return (
    <footer className={s.footer}>
      <div className={s.container}>
        <div className={s.footerTop}>
          <div className={s.escortsWrapper}>
            <div className={s.footerLogo}>
              <Logo />
            </div>
            <p>
              {callToActionFooterSiteDescription ||
                `
              Our escorts are beautiful, articulate, well groomed and very
              sensual. All ladies meet our strict selection criteria so that we
              can guarantee an exceptional service
              `}
            </p>
            <div className={s.escortsBtn}>
              <Button
                link={`${baseUrl}/make-a-date`}
                onClick={handleMakeDateClick}
              >
                Make a Date
              </Button>
            </div>
          </div>
          <div className={s.footerMenu}>
            {/*Hidden untill milestone 2 */}
            {/* <p className={s.footerTitle}>Girls & Services</p> */}
            <ul className={s.footerCategories}>
              {/*Hidden untill milestone 2 */}
              {/* {categories.map(el => (
              <li key={el.id}>
                <Link to={el.link}>{el.text}</Link>
              </li>
            ))} */}
            </ul>
          </div>
          <div className={s.footerContact}>
            <p className={s.footerTitle}>
              {callToActionFooterTitle || "Call us now"}
            </p>
            <p className={s.footerDesc}>
              {callToActionFooterText ||
                `
              Call us and book one (or two!) of our sexy London escorts
                `}
            </p>
            <ul className={s.footerPhones}>
              <li>
                <a className={s.phone} href={phoneUrl}>
                  <Phone />
                  {phone}
                </a>
              </li>
              <li>
                <a className={s.phone} href={whatsAppUrl}>
                  <Messenger />
                  {whatsApp}
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className={s.footerBottom}>
          <div className={s.footerBottomCopyright}>
            <div className={s.copyright}>
              Copyright © 2020. All rights reserved
            </div>
            <div className={s.terms}>
              <Link href={`${baseUrl}/terms`}>
                <a>Terms of use</a>
              </Link>
              {/* <Link to="/">Privacy Policy</Link> */}
            </div>
          </div>
          <div className={s.footerBottomText}>
            {termsFooter ||
              `
            This site is intended for adult viewing and may contain nudity.
            Enter the site only if you are legally entitled to access "Adult
            Sites" as defined by the laws of the country where you live. By
            entering this site, you confirm that: You are not entering this site
            in any official or unofficial capacity; in order to download images,
            or gain information for use in any media, or to use against the
            owner of the site. Money exchanged for legal adult services is for
            time and companionship. Anything implied or inferred on this web
            site is not to be taken as inducement for services other than this.
            Any sexual activities that take place are between consenting adults.
            If you are under 18 or do not agree with the above disclaimer
            statements, please leave this site now.
              `}
          </div>
        </div>
      </div>
    </footer>
  );
};
export default Footer;
