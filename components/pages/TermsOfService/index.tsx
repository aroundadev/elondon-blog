import React from 'react';

import Title from '@simple/Title';
import Button from '@simple/Button';

import Taxi from '@svg/terms/taxi.svg';
import Important from '@svg/terms/important.svg';
import Cancel from '@svg/terms/cancel.svg';
import Payment from '@svg/terms/payment.svg';

import s from './style.css';

const cards = [
  {
    id: '01',
    title: 'Taxi Charges',
    icon: <Taxi />,
    subtitle: 'Fees advertised for outcalls are for Central London areas only.',
    text:
      'Travelling fees, including taxi charges, apply for appointments in Greater London and all appointments after midnight. Clearly fees for incalls are unaffected.',
  },
  {
    id: '02',
    title: 'Cancellation Fees',
    icon: <Cancel />,
    subtitle: 'We do not charge for cancellation of incall appointments.',
    text: `Cancellation of меan outcall appointment will incur a fee equivalent to our escort's travel expenses.`,
  },
  {
    id: '03',
    title: 'Payment Method',
    icon: <Payment />,
    subtitle: 'Cash is the only form of payment our female escort girls will accept for both incalls and outcalls.',
    text:
      'Please pay the full amount in cash direct to the escort on arrival to prevent awkward moments later in your date. Please remember that our escorts are professionals, and as such they appreciate timeliness – it’s not nice to keep a lady waiting!',
  },
  {
    id: '04',
    title: 'Important',
    icon: <Important />,
    subtitle: 'Any money paid to an Angels of London escort is understood to be for her time and companionship only.',
    text:
      'Anything else that happens during your time together is the result of the independent actions of consenting adults.',
  },
];

const TermsOfService = props => {
  return (
    <div className={s.termsOfService}>
      <div className={s.termsOfService__head}>
        <div className={s.container}>
          <div className={s.titleWrapper}>
            <Title size="h3">Terms of Service</Title>
            <p className={s.text}>
              Escorts is committed to the highest levels of professional service, providing you with only the very best
              incall and outcall female escorts in London. We and our London escorts will always do our best to fulfil
              requests. We offer a first-class, honest and friendly escort service.
            </p>
          </div>
        </div>
      </div>
      <div className={s.termsOfService__main}>
        <div className={s.container}>
          <ul className={s.termsOfService__cards}>
            {cards.map(card => (
              <li className={s.card} key={card.id}>
                <div className={s.card__title}>
                  {card.icon}
                  <Title size="h6">{card.title}</Title>
                </div>
                <div className={s.card__subtitle}>{card.subtitle}</div>
                <div className={s.card__text}>{card.text}</div>
              </li>
            ))}
          </ul>
          <div className={s.haveQuestions}>
            <div className={s.haveQuestions__text}>
              <Title size="h6">Still have a questions?</Title>
              <p>
                If you are still unclear about any part of our terms or don’t understand the way the service works,
                please take a look at our Escort Service FAQ page for more information and guidance.
              </p>
            </div>
            <Button link="/faq" theme="light">
              {'Go to FAQ page'}
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TermsOfService;
