import React, { ReactElement, useRef, useEffect } from "react";
import Slider from "react-slick";
import Link from "next/link";
import Arrow from "@svg/sliderArrow.svg";

import Title from "@simple/Title";

import s from "./style.module.css";
import { Post } from "models/Post";

type Props = {
  posts: Post[];
};

const BlogSlider = ({ posts }: Props): ReactElement => {
  const settings = {
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    speed: 500,
    fade: true,
    arrows: false,
  };

  const slider = useRef(null);
  return (
    <section className={s.slider}>
      <div className={s.container}>
        <div className={s.sliderWrapper}>
          <Slider ref={slider} {...settings}>
            {posts.map(
              ({ title, slug, excerpt, thumbnail, author, date }, i) => {
                return (
                  <div key={title}>
                    <div className={s.slide}>
                      <div className={s.slideContent}>
                        <div className={s.slideInfo}>
                          <div className={s.date}>{date}</div>
                          <div className={s.author}> by {author}</div>
                        </div>
                        <div className={s.slideTitle}>
                          <Title size="h5">{title}</Title>
                        </div>
                        <div className={s.slideText}>{excerpt}</div>

                        <div className={s.arrows}>
                          <div
                            className={`${s.prevArrow}`}
                            onClick={() => slider.current.slickPrev()}
                          >
                            <Arrow />
                          </div>
                          <div
                            className={`${s.nextArrow}`}
                            onClick={() => slider.current.slickNext()}
                          >
                            <Arrow />
                          </div>
                        </div>
                      </div>
                      <div className={s.slideImage}>
                        <Link href={`/post/${slug}`}>
                          <a>
                            <img src={thumbnail} alt={title} />
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                );
              }
            )}
          </Slider>
        </div>
      </div>
    </section>
  );
};

export default BlogSlider;
