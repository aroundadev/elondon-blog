import React, { useEffect, useState } from "react";
import { BLOCKS, INLINES } from "@contentful/rich-text-types";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { Post } from "@models/Post";
import Title from "@simple/Title";
import s from "./style.module.css";
import { RichTextNode } from "../../../../../types/app.types";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Preloader from "@components/simple/Preloader";

const CustomTitle = (node, children) => {
  return <h2 className={s.contentTitle}>{children}</h2>;
};

const options = {
  renderNode: {
    [BLOCKS.EMBEDDED_ASSET]: (node: RichTextNode) => {
      const src = `https:${node.data.target.fields.file.url}`;
      return <img className={s.contentImage} src={src} />;
    },
    [BLOCKS.PARAGRAPH]: (node, children) => {
      return <p className={s.contentParagraph}>{children}</p>;
    },
    [BLOCKS.HEADING_1]: CustomTitle,
    [BLOCKS.HEADING_2]: CustomTitle,
    [BLOCKS.HEADING_3]: CustomTitle,
    [BLOCKS.HEADING_4]: CustomTitle,
    [BLOCKS.HEADING_5]: CustomTitle,

    [INLINES.HYPERLINK]: (node, children) => {
      const href = node.data.uri;
      return (
        <a target="_blank" className={s.contentLink} href={href}>
          {children}
        </a>
      );
    },
  },
};

interface Props {
  data: Post;
}

const PostContent = ({ data }: Props) => {
  const {
    title,
    author,
    date,
    headerImage,
    category,
    content: contentStr,
  } = data;
  const [imageLoaded, setImageLoaded] = useState(false);
  const content = JSON.parse(contentStr);

  useEffect(() => {
    setImageLoaded(false);
    const img = new Image();
    img.onload = () => {
      setImageLoaded(true);
    };
    img.src = headerImage;
  }, [headerImage]);

  return (
    <section className={s.post}>
      <div className={s.container}>
        <h2 className={s.category}>{category}</h2>
        <div className={s.titleWrapper}>
          <Title size="h2">{title}</Title>
        </div>
      </div>
      <div className={s.postBackground}>
        {/*}<LazyLoadImage src={thumbnail} height={200} width={600} />{*/}

        {(imageLoaded && (
          <img
            className={s.headerImage}
            src={headerImage}
            alt="blog post background"
          />
        )) || <Preloader full={false} />}
      </div>
      <div className={s.container}>
        <div className={s.contentWrapper}>
          <div className={s.content}>
            {documentToReactComponents(content, options)}
          </div>
          <div className={s.postDate}>
            <span className={s.date}>{date}</span>
            <span>by {author}</span>
          </div>
        </div>
      </div>
    </section>
  );
};

export default PostContent;
