import React, { FC, ReactElement, useRef, useEffect } from "react";
import Slider from "react-slick";
import Link from "next/link";

import Arrow from "@svg/sliderArrow.svg";

import Title from "@simple/Title";

import s from "./style.module.css";
import { Post } from "@models/Post";
import { PageSettings } from "@types/app";

type Props = {
  posts: Post[];
  title: string;
};

const PostSlider: FC<Props> = ({ posts, title }: Props): ReactElement => {
  const slider = useRef(null);

  const settings = {
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: true,
    speed: 300,
    arrows: false,
    responsive: [
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
    ],
  };

  if (posts.length === 0) return null;
  return (
    <section className={s.slider}>
      <div className={s.container}>
        <div className={s.titleWrapper}>
          <Title>{title}</Title>
        </div>
        <div className={s.arrows}>
          <div
            className={`${s.prevArrow}`}
            onClick={() => slider.current.slickPrev()}
          >
            <Arrow />
          </div>
          <div
            className={`${s.nextArrow}`}
            onClick={() => slider.current.slickNext()}
          >
            <Arrow />
          </div>
        </div>
        <div className={s.sliderWrapper}>
          <Slider ref={slider} {...settings}>
            {posts.map(
              ({ title, author, thumbnail, slug, category, date }, i) => (
                <Link href={`/post/${slug}`}>
                  <a>
                    <div key={i} className={s.post}>
                      <div className={s.postImage}>
                        <img src={`${thumbnail}?h=600`} alt={author} />
                        <span className={s.postTag}>{category}</span>
                      </div>
                      <div className={s.postContent}>
                        <div>
                          <div className={s.postTitle}>{title}</div>
                          <div className={s.postInfo}>
                            <div className={s.postDate}>{date}</div>
                            <div className={s.postAuthor}>by {author}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>
                </Link>
              )
            )}
          </Slider>
        </div>
      </div>
    </section>
  );
};

export default PostSlider;
