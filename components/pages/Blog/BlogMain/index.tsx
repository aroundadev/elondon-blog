import { Post } from "@models/Post";
import BlogSlider from "@pages/Blog/BlogSlider";
import BlogPosts from "@pages/Blog/BlogPosts";
import BlogMore from "@pages/Blog/BlogMore";
import GirlsWaiting from "components/sections/GirlsWaiting";
import { Category } from "@models/Category";

interface Props {
  posts: Post[];
  slideshow: Post[];
  categories: Category[];
  header?: React.ReactElement;
  pagination?: React.ReactElement;
}
export default function BlogMain({
  posts,
  slideshow,
  categories,
  header,
  pagination = null,
}: Props) {
  return (
    <>
      {slideshow && slideshow.length > 0 && <BlogSlider posts={slideshow} />}

      <BlogPosts
        posts={posts}
        categories={categories}
        header={header}
        pagination={pagination}
      />
      <BlogMore />
      <GirlsWaiting />
    </>
  );
}
