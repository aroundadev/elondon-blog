import React, { ReactElement } from "react";
import Link from "next/link";
import s from "./style.module.css";
import { Post } from "models/Post";
import { Category } from "@models/Category";
import { PageType } from "../../../../types/app.types";
import { LazyLoadImage } from "react-lazy-load-image-component";

type Props = {
  posts: Post[];
  categories: Category[];
  header: React.ReactElement;
  pagination: React.ReactElement;
};

const BlogPosts = ({
  posts,
  header = null,
  pagination = null,
}: Props): ReactElement => {
  return (
    <section className={s.blogPosts}>
      <div className={s.container}>
        {header}
        <div className={s.blogPostsList}>
          {posts.map(
            (
              { title, excerpt, date, author, category, thumbnail, slug },
              id
            ) => (
              <div key={id} className={s.post}>
                <div className={s.postImage}>
                  <Link href={`/post/[slug]`} as={`/post/${slug}`}>
                    <a>
                      <LazyLoadImage src={thumbnail} />
                      {/*} <img src={`${thumbnail}?h=600`} alt={author} /> {*/}
                    </a>
                  </Link>
                  {category && (
                    <span key={category} className={s.postTag}>
                      {category}
                    </span>
                  )}
                </div>
                <div className={s.postContent}>
                  <div>
                    <div className={s.postTitle}>{title}</div>
                    <div className={s.postInfo}>
                      <div className={s.postDate}>{date}</div>
                      <div className={s.postAuthor}>by {author}</div>
                    </div>
                    <div className={s.postText}>{excerpt}</div>
                  </div>
                  <Link href={`/post/[slug]`} as={`/post/${slug}`}>
                    <a className={s.postLink}>Read more</a>
                  </Link>
                </div>
              </div>
            )
          )}
        </div>
        {pagination}
        {/*}
        <div className={s.paginationContainer}>
          <Pagination
            className={s.pagination}
            defaultCurrent={1}
            total={totalPosts}
            pageSize={postsPerPage}
            onChange={handleChange}
            current={currentPage}
            itemRender={itemRender.bind(null, "category", totalPages)}
          />
        </div>
      {*/}
      </div>
    </section>
  );
};

function itemRender(
  pageType: PageType,
  total: number,
  current: number,
  type: string,
  element: React.ReactNode
) {
  let href, as: string;

  if (pageType === "main") {
    href = `?p=${current}`;
    as = null;
  } else if (pageType === "category") {
    href = `/category/[slug]?p=${current}`;
    as;
  }

  if (type === "page") {
    return (
      <Link href={`?p=${current}`} as={null}>
        <a className={s.paginationItem}>{current}</a>
      </Link>
    );
  }

  if (type === "jump-prev" || type === "jump-next") {
    return <span className={s.paginationItem}>{"..."}</span>;
  }

  if (type === "prev") {
    if (current <= 0) return null;
    return (
      <Link href={`?p=${current}`}>
        <a className={`${s.paginationItem} ${s.paginationBtn} ${s.next}`}>
          Previous
        </a>
      </Link>
    );
  }
  if (type === "next") {
    if (current > total) return null;
    return (
      <Link href={`?p=${current}`}>
        <a className={`${s.paginationItem} ${s.paginationBtn} ${s.next}`}>
          Next
        </a>
      </Link>
    );
  }

  return element;
}

export default BlogPosts;
