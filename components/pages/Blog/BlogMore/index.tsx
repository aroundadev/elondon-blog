import React, { FC, SyntheticEvent, useState } from "react";
import CircleArrow from "@public/svg/circle-arrow.svg";
import Title from "@simple/Title";
import Input from "@simple/Input";
import Button from "@simple/Button";
import WelcomeImage from "@svg/welcome.svg";
import InfoModal from "@simple/InfoModal";
import s from "./style.module.css";
import { PageSettings } from "@types/app";

const isValidEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

declare const NEXT_PUBLIC_MAILCHIMP_API_BASE_URL: string;

interface Props {
  pageSettings: PageSettings;
}

const BlogMore: FC<Props> = ({ pageSettings }) => {
  const [email, setEmail] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [error, setError] = useState<{ show: boolean; message?: string }>({
    show: false,
    message: "",
  });
  const [loading, setLoading] = useState(false);
  const onEmailChange = (e: SyntheticEvent<HTMLInputElement>): void => {
    setEmail(e.currentTarget.value);
    setError({ show: false });
  };

  const { subscribeTitle, subscribeText, subscribeButton } = pageSettings;

  return (
    <section className={s.blogMore}>
      <div className={s.container}>
        <div className={s.content}>
          <div className={s.titleWrapper}>
            <Title size="h3">{subscribeTitle || `Hungry for more?`}</Title>
          </div>
          <p>
            {subscribeText ||
              `Join our newsletter to stay updated on new girls and special offers!`}
          </p>
          <form
            className={s.form}
            onSubmit={async (e: SyntheticEvent<HTMLFormElement>) => {
              e.preventDefault();
              if (!isValidEmail.test(email)) {
                setError({
                  show: true,
                  message: "Enter valid email",
                });
                return;
              }
              setLoading(true);
              let response = await fetch(
                `${process.env.NEXT_PUBLIC_MAILCHIMP_API_BASE_URL}/subscribe?email=${email}`
              );

              const data = (await response.json()) as {
                subscriptionResult?: {};
                error?: {};
              };
              setLoading(false);
              if (data.error) {
                setError({
                  show: true,
                  message:
                    "Mailchimp subscription error. Maybe You allready are in the list",
                });
              } else {
                setShowModal(true);
                setError({ show: false });
              }
            }}
          >
            <Input
              type="email"
              id="subscribe"
              name="subscribe"
              placeholder="Enter email address"
              value={email}
              onChange={onEmailChange}
            />
            <div className={s.formBtn}>
              {" "}
              <Button buttonType="submit" size="sm">
                {subscribeButton || "Subscribe"}
                {loading && <CircleArrow className={s.loadingIcon} />}
              </Button>
            </div>
          </form>
          <p className={s.error}>{error.show && error.message}</p>
        </div>
        <div className={s.image}>
          <WelcomeImage />
        </div>
      </div>
      {showModal && (
        <InfoModal
          title="Thank You!"
          message="Your email has been added to the subscription list"
          buttonText="OK"
          handleClose={() => setShowModal(false)}
        />
      )}
    </section>
  );
};
export default BlogMore;
