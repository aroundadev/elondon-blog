import React, { useState } from 'react';

import Title from '@simple/Title';
import Arrow from '@svg/faq/arrow.svg';
import Phone from '@svg/phone.svg';
import Messenger from '@svg/messenger.svg';

import s from './style.css';

const accordion = [
  {
    id: '01',
    question: 'What are incalls and outcalls?',
    answer:
      'If you book an escort for an outcall appointment, she comes to you – sometimes to your home, but more usually to your hotel room. For an incall you visit her at her London apartment. Outcall appointments tend to be a little longer than incalls, because our clients often like to take their escort girls for dinner, or to some sort of show or event. The minimum booking is for one hour – beyond that, you can hire an escort (or bisexual escorts) for as long as you want.',
  },
  {
    id: '02',
    question: 'How far in advance do I need to book?',
    answer:
      'You can make a booking on the same day, you can book online or by phone. If you would like to book in advance that is also an option, but please note advance Online Bookings will still need to be confirmed on the date! Booking in advance is a good option to secure a date with the escort girl of your choice at the time most convenient to you.',
  },
  {
    id: '03',
    question: 'What payment methods do the escorts accept?',
    answer: 'Cash is the ONLY form of payment our sexy escort girls will accept.',
  },
  {
    id: '04',
    question: 'Can I pay using another currency?',
    answer:
      'Not unless prior agreement has been made. British Sterling is the preferred choice, however we will always try our best to accept all major currencies. Exchange rate calculation will be at the discretion of the admin arranging your date.',
  },
  {
    id: '05',
    question: 'Is all this legal? Am I breaking the law?',
    answer:
      'Yes it is legal, and, no, you’re definitely not breaking the law. We are simply representing freelance individuals who take payment in exchange for spending their time with you. Lots of agencies do that, to provide temporary office staff, supply teachers and babysitters. We do it for escorts. What the girls do when they are with you is their business and yours – it’s nothing to do with us. As for you, as long as you don’t pay anyone under the age of 18 to be an escort for yourself, you’re fine. All our escorts are well over age and have provided us with proof of that.',
  },
  {
    id: '06',
    question: 'Can I book more than one girl at a time?',
    answer:
      'Of course. Not all of our escort girls are actively bisexual, but those that are have a note on their profile page, along with information about their preferred partners.',
  },
  {
    id: '07',
    question: 'Can me and my partner see an escort together?',
    answer:
      'Yes, some of our escort girls enjoy meeting couples as well as singles! However not all female escorts are so open minded, you must inform us prior to booking that you wish to include a third party so that we can ensure your selected escort girl is happy with the arrangements. Often there may be a surcharge added, amount is at the escorts discretion.',
  },
  {
    id: '08',
    question: 'Are the girls’ fees negotiable?',
    answer:
      'Sorry, they’re not. The ladies we represent are professional escorts. They use their professional skills to make a living in the same way your lawyer or accountant does – would you haggle with them? The only circumstances under which negotiation might take place is if you were taking one of our escorts out for a longer period than is listed on her scale of fees. In such a situation we would expect to discuss the price with you.',
  },
  {
    id: '09',
    question: 'Are the pictures on the girls’ profile pages real?',
    answer:
      'Absolutely. There are some escort agencies out there who fob you off with fake pictures – we don’t. We have a policy of always showing escorts’ faces in their galleries, so if the girl who turns up isn’t the girl you were expecting there can be no arguments. We respect our clients, we don’t cheat them.',
  },
  {
    id: '10',
    question: 'I have a special request – is that OK?',
    answer:
      'We’re quite happy to pass on our client’s requests to our girls. If it’s fairly simple – “could she bring her bisexual girlfriend?” – we might be able to help you on the spot. Otherwise, we’re not able to enter into negotiation about the personal services you may receive during your incall or outcall experience. That’s down to you and her. We are, however, quite content to pass on special requests.',
  },
  {
    id: '11',
    question: 'Help! My question isn’t answered here!',
    answer:
      'That’s not a problem: we’re committed to the highest possible level of customer service. Contact Us and we’ll do our best to answer your question!',
  },
];

const FAQ = props => {
  const [actives, setActives] = useState({ '01': true });

  const handleToggle = (id: string) => {
    if (actives[id]) {
      setActives(prevState => ({ ...prevState, [id]: false }));
    } else {
      setActives(prevState => ({ ...prevState, [id]: true }));
    }
  };
  return (
    <div className={s.faq}>
      <section className={s.faq__head}>
        <div className={s.container}>
          <div className={s.titleWrapper}>
            <Title size="h3" content={'FAQ'}>
              Frequently asked questions
            </Title>
            <p className={s.desc}>
              This sectin was created to save our time and yours, we’ve compiled a list of Frequently Asked Questions to
              help you if you’re new to hiring sexy young escort girls, or if you’re confused by anything on the site.
            </p>
          </div>
        </div>
      </section>
      <section className={s.faq__main}>
        <div className={`${s.container} ${s.faq__main__container}`}>
          <div className={s.accordion__wrapper}>
            <ul>
              {accordion.map(item => (
                <li className={`${s.item} ${actives[item.id] ? s.active : ''}`} key={item.id}>
                  <button type="button" className={s.item__question} onClick={() => handleToggle(item.id)}>
                    {item.question}
                    <span className={s.arrow}>
                      <Arrow />
                    </span>
                  </button>
                  <p className={s.item__answer}>{item.answer}</p>
                </li>
              ))}
            </ul>
          </div>
          <div className={s.gotQuestions}>
            <div>
              <Title size="h5">Still got questions?</Title>
              <p>In case if you still got questions just make a call and our managers will try to help you.</p>
            </div>
            <div className={s.phones}>
              <div className={s.phone}>
                <Phone />
                <a href="tel:+079 079 00666">+079 079 00666</a>
              </div>
              <div className={s.phone}>
                <Messenger />
                <a href="tel:+(44) 079 079 00666">+(44) 079 079 00666</a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default FAQ;
