import React from "react";
import Title from "@simple/Title";
import Lips from "@svg/lips.svg";
import Hours from "@svg/hours.svg";
import Diamond from "@svg/diamond.svg";

import s from "./style.module.css";

const AboutUs = (props) => (
  <section className={s.aboutUs}>
    <div className={s.container}>
      <div className={s.column}>
        <div className={s.titleWrapper}>
          <Title size="h2">About us</Title>
          <p>
            Escorts London is the home of premium adult entertainment, created
            for the modern Ladies and Gentlemen who appreciate the finest things
            in life. We believe that you deserve the best, and that’s why we
            pride our self to only work with the finest ladies that the industry
            has to offer.
          </p>
        </div>
        <div className={s.card}>
          <div className={s.cardIcon}>
            <Lips />
          </div>
          <div className={s.cardTitle}>
            <Title size="h5">Quick and convenient</Title>
          </div>
          <p>
            Booking an intimate companion with London Escorts is a quick and
            hassle-free process. We offer an out call service, so you don’t even
            have to leave the house. Or maybe you prefer our in-call service,
            simply search through our profiles until you find a convenient
            location. To book, give us a call, or send us a WhatsApp message,
            it’s that simple.
          </p>
        </div>
        <div className={s.card}>
          <div className={s.cardIcon}>
            <Lips />
          </div>
          <div className={s.cardTitle}>
            <Title size="h5">Discretion</Title>
          </div>
          <p>
            At Escorts London, we value the privacy and discretion of our
            clients. We understand the importance of trust, and we follow a
            strict set of rules and regulations to ensure that your information
            is kept private.
          </p>
        </div>
      </div>
      <div className={s.column}>
        <div className={`${s.card} ${s.cardSpace}`}>
          <div className={s.cardIcon}>
            <Hours />
          </div>
          <div className={s.cardTitle}>
            <Title size="h5">10am - 2am</Title>
          </div>
          <p>
            Lunchtime madness or a late-night snack? Our girls work until late,
            why not make one last stop on your way home.
          </p>
        </div>
        <div className={s.card}>
          <div className={s.cardIcon}>
            <Diamond />
          </div>
          <div className={s.cardTitle}>
            <Title size="h5">Our escorts</Title>
          </div>
          <p>
            Our hand-picked team of vetted escorts ensures that we provide the
            highest quality of sexual playmates. We also use an in-house
            photographer to provide recent and up-to-date pictures, giving a
            complete representation of the babe that you can expect to meet.
          </p>
        </div>
      </div>
    </div>
  </section>
);
export default AboutUs;
