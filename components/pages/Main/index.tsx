import React, { ReactElement } from 'react';

import GirlsWaiting from '@sections/GirlsWaiting';

import Welcome from './Welcome';
import AboutUs from './AboutUs';
import LatestPosts from './LatestPosts';
import EscortsListContainer from '@containers/EscortsListContainer';
import RecommendedContainer from '@containers/RecommendedContainer';
import Hero from './Hero';
import HeroContainer from '@containers/HeroContainer';
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface Props {}
const Main = ({}: Props): ReactElement => (
  <>
    <HeroContainer />
    <EscortsListContainer />
    <Welcome />
    <AboutUs />
    <RecommendedContainer title="Recommended" />
    {/* Hidden until Milestone 2 */}
    {/* <LatestPosts /> */}
    <GirlsWaiting />
  </>
);
Main.propTypes = {};
export default Main;
