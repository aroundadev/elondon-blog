import React from "react";

import Title from "@simple/Title";
import WelcomeImage from "@svg/welcome.svg";

import s from "./style.css";

const Welcome = props => (
  <section className={s.welcome}>
    <div className={s.container}>
      <div className={s.content}>
        <div className={s.titleWrapper}>
          <Title size="h2">Welcome to our agency!</Title>
        </div>
        <p>
          We have handpicked London’s hottest and most talented babes. The kind
          of babes that most men only ever see from behind a screen. But you
          don't have to be like most men, our ladies want to meet you, and serve
          to your sexual desires. Just sit back, and enjoy the ride.
        </p>
      </div>
      <div className={s.image}>
        <WelcomeImage />
      </div>
    </div>
  </section>
);
export default Welcome;
