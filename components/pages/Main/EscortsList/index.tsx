import React, { useState, ReactElement, useEffect } from 'react';

import Title from '@simple/Title';
import Button from '@simple/Button';
import Card from '@simple/Card';
import EscortNotFound from '@simple/EscortNotFound';

import s from './style.css';
import { Escort } from '@store/escorts';
import Search from '@simple/Search';
import SearchContainer from '@containers/SearchContainer';

interface Props {
  escorts: Escort[];
  loadMore: (p: { limit: number; skip: number }) => void;
  total: number;
  skip: number;
  limit: number;
  showMoreButton: boolean;
  showFiltered: boolean;
}

export default function EscortsList({
  escorts,
  loadMore,
  total,
  limit,
  skip,
  showMoreButton,
  showFiltered,
}: Props): ReactElement | null {
  if (!escorts || escorts.length === 0) {
    if (showFiltered) {
      return <EscortNotFound />;
    } else {
      return null;
    }
  }

  return (
    <section className={s.londonEscorts}>
      <div className={s.container}>
        <div className={s.contentWrapper}>
          <div className={s.titleWrapper}>
            <Title content="Naughty and Discrete">London Escorts</Title>
          </div>
          <SearchContainer />
        </div>
        <div className={s.cardsList}>
          {escorts.map(
            ({
              id,
              title,
              imageUrl,
              location,
              incallPrice,
              outcallPrice,
              newTag,
              recommendedTag,
            }) => (
              <div key={title}>
                <Card
                  id={id}
                  title={title}
                  imageUrl={imageUrl}
                  location={location?.name}
                  incallPrice={incallPrice}
                  outcallPrice={outcallPrice}
                  newTag={newTag}
                  recommendedTag={recommendedTag}
                />
              </div>
            ),
          )}
        </div>
        <div className={s.btnWrapper}>
          {/*showMoreButton*/ true && (
            <Button
              style={{
                backgroundColor: showMoreButton ? '#FA1D52' : '#E7E7E7',
                color: showMoreButton ? 'white' : 'black',
              }}
              onClick={
                showMoreButton
                  ? loadMore.bind(null, { limit, skip })
                  : () =>
                      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
              }
              size="md"
            >
              {showMoreButton ? 'Show more' : 'Scroll to top'}
            </Button>
          )}
        </div>
      </div>
    </section>
  );
}
