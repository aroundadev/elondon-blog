import React, { ReactElement } from 'react';

import s from './style.css';

type Props = {
  label: string;
  error?: string | null;
  children: React.ReactNode;
  className?: string;
};

const Field = ({ label, error, children, className }: Props): ReactElement => {
  return (
    <div className={`${s.field} ${className ? className : ''}`}>
      <div className={s.fieldText}>
        <label className={s.label}>{label}</label>
        {error && <div className={s.error}>{error}</div>}
      </div>
      {children}
    </div>
  );
};

export default Field;
