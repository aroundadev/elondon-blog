import React from "react";
import Link from "next/link";
import s from "./style.module.css";

interface Props {
  style?: any;
  size?: string;
  theme?: string;
  link?: string;
  children: React.ReactNode;
  onClick?: (e: React.SyntheticEvent<any>) => void;
  buttonType: "submit" | "button";
}

const Button = ({
  style,
  size = "sm",
  theme = "primary",
  link = "",
  children,
  onClick,
  buttonType = "button",
}: Props) => (
  <>
    {link ? (
      <Link href={link}>
        <a
          style={style}
          onClick={onClick}
          aria-label={children[0]}
          tabIndex={-1}
          className={`
					${s.button}
					${size === "sm" ? s.sm : ""}
					${size === "md" ? s.md : ""}
					${size === "lg" ? s.lg : ""}
					${theme === "outline" ? s.outline : ""}
					${theme === "primary" ? s.primary : ""}
					${theme === "transparent" ? s.transparent : ""}
					${theme === "light" ? s.light : ""}
					${Array.isArray(children) ? s.withArrow : ""}
				`}
        >
          {children}
        </a>
      </Link>
    ) : (
      <button
        style={style}
        onClick={onClick}
        aria-label={children[0]}
        tabIndex={-1}
        type={buttonType}
        className={`
					${s.button}
					${size === "sm" ? s.sm : ""}
					${size === "md" ? s.md : ""}
					${size === "lg" ? s.lg : ""}
					${theme === "outline" ? s.outline : ""}
					${theme === "primary" ? s.primary : ""}
					${theme === "transparent" ? s.transparent : ""}
					${theme === "light" ? s.light : ""}
					${Array.isArray(children) ? s.withArrow : ""}
				`}
      >
        {children}
      </button>
    )}
  </>
);

export default Button;
