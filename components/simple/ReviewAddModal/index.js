import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

import { customSelectStyle } from '@functions/select-style.js';

import Title from '@simple/Title';
import Button from '@simple/Button';
import Rating from '@simple/Rating';
import Textarea from '@simple/Textarea';
import Phone from '@simple/Phone';

import s from './style.css';

const VISIT_TYPES = [
  { value: 'incall', label: 'Incall' },
  { value: 'outcall', label: 'Outcall' },
];

const TIMES = [
  { value: '00:00', label: '00:00' },
  { value: '01:00', label: '01:00' },
  { value: '02:00', label: '02:00' },
  { value: '03:00', label: '03:00' },
  { value: '04:00', label: '04:00' },
  { value: '05:00', label: '05:00' },
  { value: '06:00', label: '06:00' },
  { value: '07:00', label: '07:00' },
  { value: '08:00', label: '08:00' },
  { value: '09:00', label: '08:00' },
  { value: '10:00', label: '10:00' },
  { value: '11:00', label: '11:00' },
  { value: '12:00', label: '12:00' },
  { value: '13:00', label: '13:00' },
  { value: '14:00', label: '14:00' },
  { value: '15:00', label: '15:00' },
  { value: '16:00', label: '16:00' },
  { value: '17:00', label: '17:00' },
  { value: '18:00', label: '18:00' },
  { value: '19:00', label: '19:00' },
  { value: '20:00', label: '20:00' },
  { value: '21:00', label: '21:00' },
  { value: '22:00', label: '22:00' },
  { value: '23:00', label: '23:00' },
];

const DUIRATIONS = [
  { value: 'one_hour', label: '1 Hour' },
  { value: 'two_hours', label: '90 minutes' },
  { value: 'three_hours', label: 'Three hours' },
  { value: 'additional_hour', label: 'Additional hour' },
  { value: 'overnight', label: 'Overnight' },
];

const ReviewAddModal = ({ profile, handleVerification, onRequestClose }) => {
  const [values, setValues] = useState({
    rate: 5,
    visitType: '',
    time: '',
    duration: '',
    comment: '',
    phone: '',
    code: '',
  });

  const handleRateChange = value => {
    setValues({ ...values, rate: value });
  };

  const handleSelectChange = (obj, e) => {
    setValues({ ...values, [e.name]: obj.value });
  };

  const handleInputChange = e => {
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });
  };

  const handleSubmit = () => {
    console.log('handleSubmit', values);
    onRequestClose();
    handleVerification();
  };

  return (
    <div className={s.root}>
      <div className={s.top}>
        <Title size="h4">{profile.title ? `Feedback to ${profile.title}` : ''}</Title>
      </div>
      <div className={s.wrapper}>
        <div className={s.formControl}>
          <label>Rate</label>
          <Rating rate={values.rate} withRate={false} size="lg" onChange={handleRateChange} />
        </div>
        <div className={`${s.formControl} ${s.formControlInline}`}>
          <div className={s.formInlineItem}>
            <label>Visit type</label>
            <div className={s.selectWrapper}>
              <Select
                closeMenuOnSelect={true}
                isSearchable={false}
                styles={customSelectStyle}
                placeholder="Visit type"
                options={VISIT_TYPES}
                hideSelectedOptions={false}
                defaultValue={values.visitType}
                onChange={handleSelectChange}
                name="visitType"
              />
            </div>
          </div>
          <div className={s.formInlineItem}>
            <label>Time</label>
            <div className={s.selectWrapper}>
              <Select
                closeMenuOnSelect={true}
                isSearchable={false}
                styles={customSelectStyle}
                placeholder="Time"
                options={TIMES}
                hideSelectedOptions={false}
                defaultValue={values.time}
                onChange={handleSelectChange}
                name="time"
              />
            </div>
          </div>
          <div className={s.formInlineItem}>
            <label>Duration</label>
            <div className={s.selectWrapper}>
              <Select
                closeMenuOnSelect={true}
                isSearchable={false}
                styles={customSelectStyle}
                placeholder="Duration"
                options={DUIRATIONS}
                hideSelectedOptions={false}
                defaultValue={values.duration}
                onChange={handleSelectChange}
                name="duration"
              />
            </div>
          </div>
        </div>
        <div className={s.formControl}>
          <Textarea
            onChange={handleInputChange}
            placeholder="Enter your comment"
            name="comment"
            id="comment"
            value={values.comment}
            label="Comment"
          />
        </div>
        <div className={`${s.formControl} ${s.formControlInlinePhone}`}>
          <div className={s.formInlineItem}>
            <Phone
              label="Phone number"
              onChange={handleInputChange}
              value={values.phone}
              name="phone"
              code={values.code}
              onCodeChange={handleSelectChange}
              codeName="code"
              id="phoneNumber"
            />
          </div>
          <div className={s.formInlineItem}>
            <div className={s.infoText}>All information which you provide is confidential!</div>
          </div>
        </div>
      </div>
      <div className={s.bottom}>
        <div className={s.bottomText}>
          After pressing “Submit” an SMS with verification code will be sent to the mobile number you have provided.
        </div>
        <div className={s.buttonControl} onClick={handleSubmit}>
          <Button size="md">Submit</Button>
        </div>
      </div>
    </div>
  );
};

ReviewAddModal.propTypes = {
  profile: PropTypes.object.isRequired,
  handleVerification: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired,
};

export default ReviewAddModal;
