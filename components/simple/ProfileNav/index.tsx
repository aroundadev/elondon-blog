import React, { ReactElement } from "react";
import Link from "next/link";
import ArrowRight from "@svg/arrow-right.svg";
import ArrowLeft from "@svg/arrow-left.svg";
import s from "./style.module.css";

interface Props {
  prev: string;
  next: string;
  cleanCurrentProfileData: () => void;
}

export default function ProfileNav({
  prev,
  next,
  cleanCurrentProfileData,
}: Props): ReactElement {
  return (
    <>
      {prev && (
        <Link prefetch={false} href={`/post/${prev}`}>
          <a className={`${s.navBtn} ${s.left}`}>
            <ArrowLeft />
          </a>
        </Link>
      )}
      {next && (
        <Link prefetch={false} href={`/post/${next}`}>
          <a className={`${s.navBtn} ${s.right}`}>
            <ArrowRight />
          </a>
        </Link>
      )}
    </>
  );
}
