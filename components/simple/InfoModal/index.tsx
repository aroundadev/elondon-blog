import React from "react";
import Button from "@simple/Button";

import s from "./style.module.css";

interface Props {
  handleClose(): void;
  title: string;
  message: string;
  buttonText: string;
}

const InfoModal = ({ handleClose, title, message, buttonText }) => {
  return (
    <div className={s.modal}>
      <div className={s.modalWrapper} onClick={(e) => e.stopPropagation()}>
        <div className={s.modalTitle}>
          <span>{title}</span>
        </div>
        <p className={s.modalDesc}>{message}</p>
        <Button onClick={handleClose}>{buttonText}</Button>
      </div>
    </div>
  );
};

export default InfoModal;
