import React, { ReactElement } from "react";
import PropTypes from "prop-types";
import { useMediaQuery } from "react-responsive";

import Link from "next/link";

import Location from "@svg/location.svg";

import s from "./style.module.css";

const baseUrl = process.env.NEXT_PUBLIC_BASE_URL;

interface Props {
  id: string;
  title?: string;
  imageId?: string;
  imageUrl?: string;
  incallPrice?: string;
  outcallPrice?: string;
  location?: string;
  newTag?: boolean;
  recommendedTag?: boolean;
  onClick: () => void;
}

const Card = ({
  imageUrl,
  title,
  location,
  incallPrice,
  outcallPrice,
  newTag,
  recommendedTag,
}: Props): ReactElement => {
  const isMobile = useMediaQuery({ query: "(max-width: 600px)" });

  return (
    <a
      className={s.cardContainer}
      href={`${baseUrl}/profile/${title?.toLowerCase()}`}
    >
      <div className={s.imageContainer}>
        <img className={s.image} src={`${imageUrl}?h=400`} alt={title} />
        <div className={s.labels}>
          {newTag ? <span className={`${s.label} ${s.new}`}> New </span> : null}
          {recommendedTag ? (
            <span className={`${s.label} ${s.recommended}`}> Recommended </span>
          ) : null}
        </div>
      </div>
      <div className={s.details}>
        <div className={s.name}>{title}</div>
        <div className={`${s.location}`}>
          <Location />
          {location}
        </div>
        <div className={s.prices}>
          {!!parseInt(incallPrice || "0") && (
            <div className={s.price}>
              <span className={s.priceText}>
                {isMobile ? "In:" : "Incall:"}
              </span>
              <span className={s.priceValue}>£{incallPrice}</span>
            </div>
          )}
          {!!outcallPrice && (
            <div className={s.price}>
              <span className={s.priceText}>
                {isMobile ? "Out:" : "Outcall:"}
              </span>
              <span className={s.priceValue}>£{outcallPrice}</span>
            </div>
          )}
        </div>
      </div>
    </a>
  );
};
export default Card;
