import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Title from '@simple/Title';
import Review from '@simple/Review';
import Button from '@simple/Button';

import Star from '@svg/star.svg';

import s from './style.css';

const ReviewsInnerModal = ({ profile, reviews, handleAddReview, onRequestClose }) => {
  const handleReview = () => {
    onRequestClose();
    handleAddReview();
  };

  return (
    <div className={s.root}>
      <div className={s.top}>
        <Title size="h6" after={`${22} years old`}>
          {profile.title ? `About ${profile.title}` : ''}
        </Title>
      </div>
      <div className={s.list}>
        {_.map(reviews, review => (
          <Review key={review.id} review={review} showMoreLimit="100" className="column" />
        ))}
      </div>
      <div className={s.bottom} onClick={handleReview}>
        <Button size="sm">
          Submit review
          <Star />
        </Button>
      </div>
    </div>
  );
};

ReviewsInnerModal.propTypes = {
  profile: PropTypes.object.isRequired,
  reviews: PropTypes.array.isRequired,
  handleAddReview: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired,
};

export default ReviewsInnerModal;
