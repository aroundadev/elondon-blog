import React, { ReactElement, useState, useEffect } from 'react';
import SearchIcon from '@svg/search.svg';
import s from './style.css';
import { EscortIndexed } from '@store/types';
import { useLocation, useHistory } from 'react-router-dom';

interface Props {
  onChange: (name: string) => void;
  cleanPreviousData: () => void;
  addAllEscorts: () => void;
  escorts: EscortIndexed[];
}

export default function Search({
  onChange,
  cleanPreviousData,
  addAllEscorts,
  escorts,
}: Props): ReactElement {
  const [value, setValue] = useState<string | null>(null);

  useEffect(() => {
    // if (value && value?.length > 0) {
    //   cleanPreviousData();
    //   onChange(value);
    // } else if (value === '') {
    //   addAllEscorts();
    // }

    if (value === '') {
      addAllEscorts();
    }
  }, [value]);

  const history = useHistory();

  const handleChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const v = e.currentTarget.value;
    setValue(v);
  };

  const handleFocus = () => {
    addAllEscorts();
  };

  const handleBlur = () => {
    console.log('BLUR');
    cleanPreviousData();
    setValue(null);
  };

  const handleMouseDown = (location: string) => {
    cleanPreviousData();
    history.push(location);
  };

  const escortsFiltered = !value
    ? escorts
    : escorts.filter(escort => {
        const filter = new RegExp(`^${value}`, 'i');
        return filter.test(escort.title || '');
      });

  return (
    <div className={s.searchWrapper}>
      <div className={s.search}>
        <span className={s.searchIcon}>
          <SearchIcon className={s.searchByNameSearchIcon} />
          <span></span>
        </span>
        <input
          value={value || ''}
          onChange={handleChange}
          onFocus={handleFocus}
          onBlur={handleBlur}
          type="search"
          placeholder="Search by name..."
        />
      </div>
      <ul className={s.searchList}>
        {escortsFiltered.map((escort, i) => (
          <li key={i} className={s.searchListItem}>
            <a
              onMouseDown={handleMouseDown.bind(
                null,
                `/profile/${escort.title}`,
              )}
              href={`/profile/${escort.title}`}
              className={s.searchListLink}
            >
              {escort.title}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
}
