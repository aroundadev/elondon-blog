import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import ImageGallery from 'react-image-gallery';
import { useMediaQuery } from 'react-responsive';
import ArrowRight from '@svg/arrow-right.svg';
import ArrowLeft from '@svg/arrow-left.svg';
import ArrowFullRight from '@svg/gallery-arrow-right.svg';
import ArrowFullLeft from '@svg/gallery-arrow-left.svg';
import Close from '@svg/close-burger.svg';

import s from './style.css';
import { GalleryData } from '@components/pages/ProfilePage';
import Preloader from '../Preloader';

import logo from '../../../assets/url-loader/test.png';
import logoHor from '../../../assets/url-loader/watermark/horizontal.png';
import logoVert from '../../../assets/url-loader/watermark/vertical.png';

const options = {
  init: function(img) {
    img.crossOrigin = 'anonymous';
  },
};

interface Props {
  images: GalleryData[];
}

const addWatermark = (
  inputImageUrl: string,
  horLogoUrl: string,
  vertLogoUrl: string,
) => {
  return new Promise((resolve: (d: any) => void) => {
    const tempImage = new Image();
    tempImage.crossOrigin = 'anonymous';
    tempImage.onload = () => {
      // Get input image dimensions
      const inputWidth = tempImage.width;
      const inputHeight = tempImage.height;

      //Create canvas with input image dimensions
      const cnv = document.createElement('canvas');
      cnv.width = inputWidth;
      cnv.height = inputHeight;
      const ctx = cnv.getContext('2d');

      //Draw input image on this canvas
      ctx?.drawImage(tempImage, 0, 0, inputWidth, inputHeight);

      const logoImage = new Image();
      logoImage.onload = () => {
        let logoWidth, logoHeight, posX, posY;
        if (inputWidth < inputHeight) {
          logoWidth = logoImage.width;
          logoHeight = inputHeight;
          posX = 0;
          posY = 0;
        } else {
          logoWidth = inputWidth;
          logoHeight = logoImage.height;
          posX = 0;
          posY = inputHeight - logoHeight;
        }
        ctx?.drawImage(logoImage, posX, posY, logoWidth, logoHeight);
        resolve(cnv.toDataURL());
      };

      if (inputWidth < inputHeight) {
        logoImage.src = vertLogoUrl;
      } else {
        logoImage.src = horLogoUrl;
      }
    };
    tempImage.src = inputImageUrl;
  });
};

const Gallery = ({ images }: Props) => {
  const [isFullScreen, setFullScreen] = useState(false);
  const [wmImages, setWmImages] = useState<GalleryData[]>([]);

  useEffect(() => {
    addWatermark(images[0].original, logoHor, logoVert)
      .then(data => {
        setWmImages([{ original: data, thumbnail: data }]);
      })
      .then(() => {
        images.splice(1).forEach((image, i) => {
          addWatermark(image.original, logoHor, logoVert).then(data => {
            setWmImages(wmImages => [
              ...wmImages,
              { original: data, thumbnail: data },
            ]);
          });
        });
      });
  }, []);

  let imageGalleryRef = useRef();

  const isMobile = useMediaQuery({ maxWidth: 767 });

  const onScreenChange = fullScreenElement => {
    setFullScreen(!!fullScreenElement);
  };

  const renderLeftNav = (onClick, disabled) => {
    return (
      <button
        className={`${s.nav} ${s.leftNav}`}
        disabled={disabled}
        onClick={onClick}
      >
        {isFullScreen ? <ArrowFullLeft /> : <ArrowLeft />}
      </button>
    );
  };

  const renderRightNav = (onClick, disabled) => {
    return (
      <button
        className={`${s.nav} ${s.rightNav}`}
        disabled={disabled}
        onClick={onClick}
      >
        {isFullScreen ? <ArrowFullRight /> : <ArrowRight />}
      </button>
    );
  };

  const renderFullscreenButton = (onClick, isFullscreen) => {
    return (
      isFullscreen && (
        <button className={s.fullScreenClose} onClick={onClick}>
          <Close />
        </button>
      )
    );
  };

  const onImageClick = () => {
    imageGalleryRef.fullScreen();
  };

  const showThumbnails = isFullScreen || !isMobile;

  // if (images.length === 0) return <Preloader />;
  if (wmImages.length === 0) return <Preloader />;

  return (
    <>
      <ImageGallery
        ref={i => (imageGalleryRef = i)}
        onClick={onImageClick.bind(this)}
        onScreenChange={onScreenChange.bind(this)}
        items={wmImages}
        showPlayButton={false}
        lazyLoad={true}
        renderLeftNav={renderLeftNav}
        renderRightNav={renderRightNav}
        renderFullscreenButton={renderFullscreenButton}
        showThumbnails={showThumbnails}
      />
    </>
  );
};

Gallery.defaultProps = {
  images: [],
};

Gallery.propTypes = {
  images: PropTypes.array.isRequired,
};

export default React.memo(Gallery);
