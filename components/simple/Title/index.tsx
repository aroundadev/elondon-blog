import React from "react";
import PropTypes from "prop-types";

import s from "./style.module.css";

interface Props {
  children: React.ReactNode;
  size?: string;
  content?: string;
  after?: string;
  onClick?: (e: React.SyntheticEvent<any>) => void;
}

const Title = ({ children, size, content, after, onClick }: Props) => (
  <>
    {size === "h1" ? (
      <h1 className={`${s.title} ${s.h1}`}>{children}</h1>
    ) : null}
    {size === "h2" ? (
      <h2 className={`${s.title} ${s.h2}`}>{children}</h2>
    ) : null}
    {size === "h3" ? (
      <h3 className={`${s.title} ${s.h3}`} aria-label={content}>
        {children}
      </h3>
    ) : null}
    {size === "h4" ? (
      <h4 className={`${s.title} ${s.h4}`}>{children}</h4>
    ) : null}
    {size === "h5" ? (
      <h5 className={`${s.title} ${s.h5}`}>{children}</h5>
    ) : null}
    {size === "h6" ? (
      <h6 className={`${s.title} ${s.h6}`}>{children}</h6>
    ) : null}
    {after.length ? (
      <span
        className={`${s.afterTitle} ${onClick && s.clicked}`}
        onClick={onClick && onClick}
      >{`${after}`}</span>
    ) : null}
  </>
);
Title.defaultProps = {
  size: "h3",
  content: "",
  after: "",
};
Title.propTypes = {
  children: PropTypes.any.isRequired,
  size: PropTypes.string,
  content: PropTypes.string,
  after: PropTypes.string,
};
export default Title;
