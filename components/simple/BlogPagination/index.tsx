import React, { useMemo, useContext, useCallback } from "react";
import Link from "next/link";
import Pagination from "rc-pagination";
import AppContext from "@lib/context";
import s from "./style.module.css";
import { PageType } from "../../../types/app.types";

interface Props {
  pageType?: PageType;
  slug?: string;
}
export default function BlogPagination({ pageType, slug }: Props) {
  const { postsPerPage, totalPosts, setCurrentPage, currentPage } = useContext(
    AppContext
  );
  const totalPages = useMemo(() => Math.ceil(totalPosts / postsPerPage), []);

  const handleChange = (pageNumber: number) => {
    setCurrentPage(pageNumber);
  };

  const render = useCallback(function itemRender(
    current: number,
    type: string,
    element: React.ReactNode
  ) {
    let href: string;
    let as: string;

    if (pageType === "main") {
      href = `?p=${current}`;
      as = null;
    } else if (pageType === "category") {
      href = `/category/[slug]?p=${current}`;
      as = `/category/${slug}?p=${current}`;
    }

    if (type === "page") {
      return (
        <Link href={href} as={as}>
          <a className={s.paginationItem}>{current}</a>
        </Link>
      );
    }

    if (type === "jump-prev" || type === "jump-next") {
      return <span className={s.paginationItem}>{"..."}</span>;
    }

    if (type === "prev") {
      return (
        <Link href={href} as={as}>
          <a className={`${s.paginationItem} ${s.paginationBtn} ${s.next}`}>
            Previous
          </a>
        </Link>
      );
    }
    if (type === "next") {
      return (
        <Link href={href} as={as}>
          <a className={`${s.paginationItem} ${s.paginationBtn} ${s.next}`}>
            Next
          </a>
        </Link>
      );
    }

    return element;
  },
  []);

  return (
    <div className={s.paginationContainer}>
      <Pagination
        className={s.pagination}
        defaultCurrent={1}
        total={totalPosts}
        pageSize={postsPerPage}
        onChange={handleChange}
        current={currentPage}
        itemRender={render}
      />
    </div>
  );
}
