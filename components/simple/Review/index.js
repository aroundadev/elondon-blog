import React from 'react';
import PropTypes from 'prop-types';

import Rating from '@simple/Rating';
import ReadMore from '@simple/ReadMore';

import s from './style.css';

const Review = ({ review, showMoreLimit, className, handleReview }) => {
  return (
    <div key={review.id} className={`${s.listItem} ${s[className]}`}>
      <div className={s.listItemTop} onClick={handleReview}>
        <div className={s.listInfo}>
          <div className={s.listInfoItem}>{review.type}</div>
          <div className={s.listInfoDot}></div>
          <div className={s.listInfoItem}>{review.duration}</div>
        </div>
        <Rating rate={+review.rate} readonly={true} />
      </div>
      <div className={s.listItemContent}>
        <ReadMore
          text={review.comment}
          id={`profile-review-comment-${review.id}`}
          innerElement="blockquote"
          lines={showMoreLimit}
        />
      </div>
    </div>
  );
};

Review.propTypes = {
  review: PropTypes.object.isRequired,
  showMoreLimit: PropTypes.string.isRequired,
  className: PropTypes.string,
  handleReview: PropTypes.func,
};

export default Review;
