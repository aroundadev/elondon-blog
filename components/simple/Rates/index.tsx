import React, { useState } from "react";
import PropTypes from "prop-types";
import _ from "lodash";

import Title from "@simple/Title";

import s from "./style.css";

const formatRate = (rate: string): string => {
  if (rate === "£" || rate === "$0" || rate == "€0") {
    return " - ";
  } else {
    let rateNum: number;
    let fRate: string;

    try {
      const sign = rate[0];
      const n = rate.replace(new RegExp("\\$|£|€", "i"), "");
      rateNum = parseFloat(n);
      rateNum = Math.round(rateNum);
      fRate = sign + rateNum.toString();
    } catch {
      fRate = " - ";
    }
    return fRate;
  }
};

const Rates = ({ rates, activeCurrency }) => {
  const [currency, setCurrency] = useState(false);

  const handleNav = currency => () => {
    setCurrency(currency);
  };
  const active = currency ? currency : activeCurrency;
  const rate = rates[active];
  return (
    <div className={s.rates}>
      <div className={s.top}>
        <Title size="h6">Rates</Title>
        <nav className={s.navs}>
          {_.map(_.keys(rates), nav => (
            <button
              key={nav}
              onClick={handleNav(nav)}
              className={`${s.nav} ${nav === active && s.active} }`}
            >
              {nav}
            </button>
          ))}
        </nav>
      </div>
      <div className={s.tableWrapper}>
        <table className={s.table}>
          <thead>
            <tr>
              <th>Time</th>
              <th>Incall</th>
              <th>Outcall</th>
            </tr>
          </thead>
          <tbody>
            {_.map(
              rate.filter(r => {
                return r.time !== "Three hours";
              }),
              (rateItem, key) => {
                return (
                  <tr key={key} className={s.rateItem}>
                    <td>
                      <div className={s.time}>
                        <img
                          src={rateItem.icon}
                          className={s.icon}
                          alt={rateItem.time}
                        />
                        <span>{rateItem.time}</span>
                      </div>
                    </td>
                    <td>{formatRate(rateItem.incall)}</td>
                    <td>{formatRate(rateItem.outcall)}</td>
                  </tr>
                );
              }
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
};

Rates.defaultProps = {
  activeCurrency: "GBP"
};

Rates.propTypes = {
  rates: PropTypes.object.isRequired,
  activeCurrency: PropTypes.string
};

export default Rates;
