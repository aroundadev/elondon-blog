import { Category } from "@models/Category";
import Link from "next/link";
import React from "react";
import Title from "../Title";
import cx from "classnames";
import s from "./style.module.css";

interface Props {
  categories: Category[];
  currentCategory?: string;
  title: string;
}

export default function BlogPostsHeader({
  categories,
  currentCategory = "",
  title,
}: Props) {
  return (
    <div className={s.blogPostsNavigation}>
      <Title size="h2">{title}</Title>
      <div className={s.tagList}>
        <Link href="/">
          <a className={cx(s.tag, !currentCategory && s.current)}>All</a>
        </Link>
        {categories.map(({ name, slug }) => (
          <Link href={"/category/[slug]"} as={`/category/${slug}`}>
            <a
              key={slug}
              className={cx(s.tag, name === currentCategory && s.current)}
            >
              {name}
            </a>
          </Link>
        ))}
      </div>
    </div>
  );
}
