const path = require("path");
module.exports = {
  basePath: "/blog",
  webpack: (config) => {
    config.resolve.alias = {
      ...config.resolve.alias,
      "@": __dirname,
      "@public": path.resolve(__dirname, "public/"),
      "@svg": path.resolve(__dirname, "public/svg"),
      "@lib": path.resolve(__dirname, "lib"),
      "@components": path.resolve(__dirname, "components"),
      "@simple": path.resolve(__dirname, "components/simple"),
      "@pages": path.resolve(__dirname, "components/pages"),
      "@types": path.resolve(__dirname, "types"),
      "@models": path.resolve(__dirname, "models"),
    };

    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"],
    });

    return config;
  },
  typescript: {
    ignoreBuildErrors: true,
  },
};
