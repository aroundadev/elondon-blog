const baseUrl = process.env.NEXT_PUBLIC_BASE_URL;
const menu = [
  {
    id: "01",
    text: "Girls",
    link: `${baseUrl}`,
  },
  {
    id: "02",
    text: "Duo Girls",
    link: `${baseUrl}/duo-girls`,
  },
  //Hidden until Milestone 2
  // {
  //   id: '02',
  //   text: 'Duo',
  //   link: '/',
  // },
  // {
  //   id: '03',
  //   text: 'Services',
  //   link: '/',
  // },
  {
    id: "04",
    text: "Blog",
    link: "/",
  },
  {
    id: "05",
    text: "Terms of Service",
    link: `${baseUrl}/terms`,
  },
  {
    id: "06",
    text: "FAQ",
    link: `${baseUrl}/faq`,
  },
];
export default menu;
