const girls = [
  {
    id: '01',
    name: 'Meryditte',
    image: '/assets/images/01.jpg',
    location: 'Bayswater',
    incall: '£200',
    outcall: '£220',
    new: true,
    recommended: true,
  },
  {
    id: '02',
    name: 'Helen',
    image: '/assets/images/02.jpg',
    location: 'Bayswater',
    incall: '£200',
    outcall: '£220',
    new: true,
    recommended: true,
  },
  {
    id: '03',
    name: 'Cherry',
    image: '/assets/images/03.jpg',
    location: 'Bayswater',
    incall: '£200',
    outcall: '£220',
    new: true,
    recommended: true,
  },
  {
    id: '04',
    name: 'Carina',
    image: '/assets/images/04.jpg',
    location: 'Bayswater',
    incall: '£200',
    outcall: '£220',
    new: true,
    recommended: true,
  },
  {
    id: '05',
    name: 'Ivanka',
    image: '/assets/images/05.jpg',
    location: 'Bayswater',
    incall: '£200',
    outcall: '£220',
    new: true,
    recommended: true,
  },
  {
    id: '06',
    name: 'Helen',
    image: '/assets/images/06.jpg',
    location: 'Bayswater',
    incall: '£200',
    outcall: '£220',
    new: true,
    recommended: true,
  },
  {
    id: '07',
    name: 'Helen',
    image: '/assets/images/07.jpg',
    location: 'Bayswater',
    incall: '£200',
    outcall: '£220',
    new: true,
    recommended: true,
  },
  {
    id: '08',
    name: 'Cherry',
    image: '/assets/images/08.jpg',
    location: 'Bayswater',
    incall: '£200',
    outcall: '£220',
    new: true,
    recommended: true,
  },
  {
    id: '09',
    name: 'Carina',
    image: '/assets/images/09.jpg',
    location: 'Bayswater',
    incall: '£200',
    outcall: '£220',
    new: true,
    recommended: true,
  },
  {
    id: '10',
    name: 'Ivanka',
    image: '/assets/images/10.jpg',
    location: 'Bayswater',
    incall: '£200',
    outcall: '£220',
    new: true,
    recommended: true,
  },
];

export default girls;
