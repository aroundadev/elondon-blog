const { inspect } = require("util");
const slugify = require("slugify");
const cma = require("contentful-management");
const cda = require("contentful");
const faker = require("faker");
const SPACE_ID = "rfr9tdku11ho";
const CMA_TOKEN = "CFPAT-qpbxmDcNk7A7vF36xPE-p9ncyySanJA9zSIEx6rp4YE"; //"CFPAT-RwIveuyX9DDWBRHvIW3SuNldSCvGDA90oUCn9Y3IOPU";
const CDA_TOKEN = "8k2DL9f-D1qLxqK8vtcM-b8LkUjuERd7awM4DKlqzt0"; //"ZfNWejjxiYvM4WcjUPsuWuBlEeskaHvrPaYrt5dsgnA";

const managementClient = cma.createClient({
  accessToken: CMA_TOKEN,
});

const deliveryClient = cda.createClient({
  space: SPACE_ID,
  environment: "master",
  accessToken: CDA_TOKEN,
});

(async () => {
  const assets = await deliveryClient.getAssets();
  const headerImageAssets = assets.items.filter((asset) => {
    const filter = new RegExp(/^blog-asset/);
    const isDemoAsset = filter.test(asset.fields.title);
    return isDemoAsset;
  });

  addPost(assets.items, headerImageAssets, 20);
})();

async function addPost(assets, headerImageAssets, t) {
  debugger;
  let title = faker.lorem.sentence(2 + faker.random.number(8), 3);
  title = title.slice(0, title.length - 1);
  const slug = slugify(title).toLowerCase();
  try {
    const entry = await managementClient
      .getSpace(SPACE_ID)
      .then((space) => space.getEnvironment("master"))
      .then((environment) =>
        environment.createEntry("blog", {
          fields: {
            title: {
              "en-US": t + ") " + title,
            },

            postSlug: {
              "en-US": slug,
            },

            excerpt: {
              "en-US": faker.lorem.paragraph(faker.random.number(2, 4)),
            },

            thumbnail: {
              "en-US": {
                sys: {
                  type: "Link",
                  linkType: "Asset",
                  id: getRandomAssetId(assets),
                },
              },
            },

            headerImage: {
              "en-US": {
                sys: {
                  type: "Link",
                  linkType: "Asset",
                  id: getRandomAssetId(headerImageAssets),
                },
              },
            },

            //date: {
            //  "en-US": faker.date.recent(100),
            //},

            category: {
              "en-US": {
                sys: {
                  type: "Link",
                  linkType: "Entry",
                  id:
                    faker.random.number(1) === 0
                      ? "7zfQSwEyd5ha3xspZooWWX"
                      : "7afQsS67jwrAoZozFcS5W4",
                },
              },
            },

            author: {
              "en-US": {
                sys: {
                  type: "Link",
                  linkType: "Entry",
                  id: [
                    "6i2MOOFk7FTM8aSWv1Zi9n",
                    "42ibg2zlyePXgftpzZfiC9",
                    "1TDrDRGIUD5k7qENcbS97A",
                  ][faker.random.number(2)],
                },
              },
            },

            content: {
              "en-US": {
                nodeType: "document",
                content: [
                  getRandHeading(),
                  getRandParagraph(),
                  getRandParagraph(),
                  getRandAsset(assets),
                  getRandHeading(),
                  getRandParagraph(),
                  getRandParagraph(),
                  getRandParagraph(),
                  getRandAsset(assets),
                  getRandHeading(),
                  getRandParagraph(),
                  getRandParagraph(),
                ],
                data: {},
              },
            },
          },
        })
      )
      .then((entry) => {
        return entry.publish();
      })
      .then((entry) => {
        console.log(entry);
        if (t > 0) {
          addPost(assets, headerImageAssets, --t);
        }
        return;
      });
  } catch (e) {
    console.log(e);
  }
}

const getRandLink = () => {
  return {
    nodeType: "hyperlink",
    content: [
      {
        nodeType: "text",
        value: faker.lorem.words(faker.random.number(5)),
        marks: [],
        data: {},
      },
    ],
    data: {
      uri: "http://google.com",
    },
  };
};

const getRandHeading = () => {
  return {
    nodeType: "heading-2",
    content: [
      {
        nodeType: "text",
        value: faker.lorem.sentence(5, 10),
        marks: [],
        data: {},
      },
    ],
    data: {},
  };
};

const getRandParagraph = () => ({
  nodeType: "paragraph",
  content: [
    {
      nodeType: "text",
      value: faker.lorem.paragraph(faker.random.number(12, 34)),
      marks: [],
      data: {},
    },
  ],
  data: {},
});

const getRandomAssetId = (assets) => {
  console.log(assets);
  const i = faker.random.number(assets.length - 1);
  const id = assets[i].sys.id;
  return id;
};

const getRandAsset = (assets) => {
  const id = getRandomAssetId(assets);
  return {
    nodeType: "embedded-asset-block",
    content: [],
    data: {
      target: {
        sys: {
          type: "Link",
          linkType: "Asset",
          id,
        },
      },
    },
  };
};
