module.exports = function (migration) {
  const blogSettings = migration.createContentType("blogSettings", {
    name: "Blog Settings",
    description: "Blog settings",
  });

  blogSettings.createField("title").name("Title").type("Symbol");

  blogSettings
    .createField("slideshowItems")
    .name("Slideshow items")
    .type("Array")
    .items({
      type: "Link",
      linkType: "Entry",
      validations: [{ linkContentType: ["blog"] }],
    });

  blogSettings
    .createField("mainPageTitle")
    .name("SEO: Main page title")
    .type("Symbol");

  blogSettings
    .createField("mainPageDescription")
    .name("SEO: Main page Description")
    .type("Text");

  blogSettings
    .createField("categoryPageTitle")
    .name("SEO: Categories page title")
    .type("Symbol");

  blogSettings
    .createField("categoryPageDescription")
    .name("SEO: Categories page Description")
    .type("Text");

  blogSettings
    .createField("postTitlePrefix")
    .name("SEO: Post title prefix")
    .type("Symbol");
};
