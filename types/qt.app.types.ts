export interface SettingsData {
  sys: SettingsDataSys;
  total: number;
  skip: number;
  limit: number;
  items: Item[];
  includes: Includes;
}

export interface Includes {
  entry: Entry[];
  asset: Asset[];
}

export interface Asset {
  sys: AssetSys;
  fields: AssetFields;
}

export interface AssetFields {
  title: string;
  file: File;
}

export interface File {
  url: string;
  details: Details;
  fileName: string;
  contentType: string;
}

export interface Details {
  size: number;
  image: Image;
}

export interface Image {
  width: number;
  height: number;
}

export interface AssetSys {
  space: ContentType;
  id: string;
  type: LinkTypeEnum;
  createdAt: Date;
  updatedAt: Date;
  environment: ContentType;
  revision: number;
  locale: Locale;
  contentType?: ContentType;
}

export interface ContentType {
  sys: ContentTypeSys;
}

export interface ContentTypeSys {
  id: string;
  type: PurpleType;
  linkType: LinkTypeEnum;
}

export enum LinkTypeEnum {
  Asset = "Asset",
  ContentType = "ContentType",
  Entry = "Entry",
  Environment = "Environment",
  Space = "Space",
}

export enum PurpleType {
  Link = "Link",
}

export enum Locale {
  EnUS = "en-US",
}

export interface Entry {
  sys: AssetSys;
  fields: EntryFields;
}

export interface EntryFields {
  title: string;
  postSlug: string;
  thumbnail: ContentType;
  excerpt: string;
  content: any;
  category: ContentType;
  author: ContentType;
}

export interface Item {
  sys: AssetSys;
  fields: ItemFields;
}

export interface ItemFields {
  title: string;
  slideshowItems: ContentType[];
  baseURL: string;
}

export interface SettingsDataSys {
  type: string;
}
