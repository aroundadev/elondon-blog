export interface ContentfulApiData {
  sys: PostDataSys;
  total: number;
  skip: number;
  limit: number;
  items: PostData[];
  includes: Includes;
}

export interface Includes {
  Entry: Entry[];
  Asset: Asset[];
}

export interface Asset {
  sys: AssetSys;
  fields: AssetFields;
}

export interface AssetFields {
  title: string;
  file: File;
}

export interface File {
  url: string;
  details: Details;
  fileName: string;
  contentType: ContentType;
}

export enum ContentType {
  ImageJPEG = "image/jpeg",
}

export interface Details {
  size: number;
  image: Image;
}

export interface Image {
  width: number;
  height: number;
}

export interface AssetSys {
  space: Author;
  id: string;
  type: LinkTypeEnum;
  createdAt: Date;
  updatedAt: Date;
  environment: Author;
  revision: number;
  locale: Locale;
  contentType?: Author;
}

export interface Author {
  sys: AuthorSys;
}

export interface AuthorSys {
  id: string;
  type: PurpleType;
  linkType: LinkTypeEnum;
}

export enum LinkTypeEnum {
  Asset = "Asset",
  ContentType = "ContentType",
  Entry = "Entry",
  Environment = "Environment",
  Space = "Space",
}

export enum PurpleType {
  Link = "Link",
}

export enum Locale {
  EnUS = "en-US",
}

export interface Entry {
  sys: AssetSys;
  fields: EntryFields;
}

export interface EntryFields {
  name?: string;
  title?: string;
  slug?: string;
}

export interface PostData {
  sys: AssetSys;
  fields: PostDataFields;
}

export interface PostDataFields {
  title: string;
  postSlug: string;
  thumbnail: Author;
  excerpt: string;
  content: any;
  category: Author;
  author: Author;
}

export interface PostDataSys {
  type: string;
}
