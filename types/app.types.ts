import postsData from "./contentful.blog.posts.json";
export type PostsData = typeof postsData;
export type PostData = typeof postsData.items[0];

const authorData = postsData.items[0].fields.author;
export type AuthorData = typeof authorData;

const categoryData = postsData.items[0].fields.category;
export type CategoryData = typeof categoryData;

//TODO: rewrite following types as above
export type ThumbnailData = typeof import("./contentful.blog.thumbnail.json");
export type ImageData = ThumbnailData;
export type AssetData = typeof import("./contentful.blog.asset.json");
export type EscortsData = typeof import("./contentful.escorts.response.json");
export type EscortData = typeof import("./contentful.escort.response.json");
export type RatesData = typeof import("./contentful.escort.rates.json");
export type LocationData = typeof import("./contentful.escort.location.json");
// end TODO

import categoryContentTypeData from "./contentful.blog.content_type.category.entries.json";
export type CategoryContentTypeData = typeof categoryContentTypeData;
export type CategoryContentTypeEntryData = typeof categoryContentTypeData.items[0];

export type RichTextNode = {
  content: any[];
  data: {
    target: AssetData;
  };
  nodeType: "embedded-asset-block";
};

export type PageType = "main" | "category" | "post";

import settingsData from "./contentful.blog.settings.json";
export type SettingsData = typeof settingsData;
export type SettingData = typeof settingsData.items[0];
const slideshowData = settingsData.items[0].fields.slideshowItems[0];
export type SlideshowData = typeof slideshowData;
const slideshowIncludesEntryData = settingsData.includes.Entry[0];
export type SlideshowIncludesEntryData = typeof slideshowIncludesEntryData;
