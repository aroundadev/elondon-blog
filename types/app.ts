export interface ContentfulApiData<T> {
  sys: ContentfulApiDataSys;
  total: number;
  skip: number;
  limit: number;
  items: EntryData<T>[];
  includes: Includes;
}

export interface ContentfulApiDataSys {
  type: string;
}

export interface EntryData<T> {
  sys: EntryDataSys;
  fields: T;
}

export interface EntryDataSys {
  space: LinkedEntry;
  id: string;
  type: string;
  createdAt: string;
  updatedAt: string;
  environment: {
    sys: Sys;
  };
  revision: number;
  contentType: {
    sys: Sys;
  };
  locale: string;
}

export type AssetField = "thumbnail" | "headerImage";

export interface LinkedEntry {
  sys: Sys;
}

export interface Sys {
  id: string;
  type: string;
  linkType: string;
}

export interface PostData {
  title: string;
  titleMetaTag: string;
  metaDescriptionTag: string;
  postSlug: string;
  thumbnail: LinkedEntry;
  headerImage: LinkedEntry;
  excerpt: string;
  content: Content;
  category: LinkedEntry;
  author: LinkedEntry;
}

export interface SettingsData {
  title: string;
  slideshowItems: LinkedEntry[];
  mainPageTitle: string;
  mainPageDescription: string;
  categoryPageTitle: string;
  categoryPageDescription: string;
  baseUrl: string;
}

export interface Includes {
  Entry: EntryData<AuthorData | CategoryData | PostData>[];
  Asset: EntryData<AssetData>[];
}

export interface AuthorData {
  name: string;
}

export interface CategoryData {
  title: string;
  slug: string;
  titleMetaTag: string;
  metaDescriptionTag: string;
}

export type CategoryResponse = ContentfulApiData<CategoryData>;

export interface Category {
  title: string;
  slug: string;
  titleMetaTag: string;
  metaDescriptionTag: string;
}

export interface AssetData {
  title: string;
  file: AssetFile;
}

export interface AssetFile {
  url: string;
  details: {
    size: number;
    image: {
      width: number;
      height: number;
    };
    fileName: string;
    contentType: string;
  };
}

export interface Content {
  nodeType: string;
  content: Content;
  data: {};
}

export interface Service {
  slug: string;
  title: string;
}
export interface MenuItem {
  slug: string;
  title: string;
  subMenu?: MenuItem[];
}

//Blog page settings

export interface PageSettingsFields {
  titleMetaTag?: string;
  metaDescriptionTag?: string;
  latestPostsTitle?: string;
  subscribeTitle?: string;
  subscribeText?: string;
  subscribeButton?: string;
  callToActionTitle?: string;
  callToActionText?: string;
  callToActionButtonWrapperTitle?: string;
  callToActionButtonWrapperText?: string;
  phone?: string;
  whatsApp?: string;
  callToActionFooterSiteDescription?: string;
  callToActionFooterTitle?: string;
  callToActionFooterText?: string;
  termsFooter?: string;
  featuredPostsTitle?: string;
  recommendedLine1: string;
  recommendedLine2: string;
}

export type PageSettingsResponse = ContentfulApiData<PageSettingsFields>;

export interface PageSettings {
  titleMetaTag?: string;
  metaDescriptionTag?: string;
  latestPostsTitle?: string;
  subscribeTitle?: string;
  subscribeText?: string;
  subscribeButton?: string;
  callToActionTitle?: string;
  callToActionText?: string;
  callToActionButtonWrapperTitle?: string;
  callToActionButtonWrapperText?: string;
  phone?: string;
  phoneUrl?: string;
  whatsApp?: string;
  whatsAppUrl?: string;
  callToActionFooterSiteDescription?: string;
  callToActionFooterTitle?: string;
  callToActionFooterText?: string;
  termsFooter?: string;
  featuredPostsTitle?: string;
  recommendedLine1: string;
  recommendedLine2: string;
}
